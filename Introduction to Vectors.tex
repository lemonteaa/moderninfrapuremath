
\documentclass[11pt]{article}

\usepackage{ifpdf}
\ifpdf 
    \usepackage[pdftex]{graphicx}   % to include graphics
    \pdfcompresslevel=9 
    \usepackage[pdftex,     % sets up hyperref to use pdftex driver
            plainpages=false,   % allows page i and 1 to exist in the same document
            breaklinks=true,    % link texts can be broken at the end of line
            colorlinks=true,
            pdftitle=My Document
            pdfauthor=My Good Self
           ]{hyperref} 
    \usepackage{thumbpdf}
\else 
    \usepackage{graphicx}       % to include graphics
    \usepackage{hyperref}       % to simplify the use of \href
\fi 

\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{amsthm}

\newtheorem{mydef}{Definition}
\newtheorem{prop}{Proposition}
\newtheorem{cor}{Corollary}
\newtheorem{rmk}{Remark}

\title{Introduction to Vector}
\author{The Author}
\date{}

\begin{document}
\maketitle

Vectors are caught in the gap between classical and modern Mathematics. In more 
elementary texts they are introduced informally, as entity with both a direction 
and a magnitude. Operators on vectors are also done intuitively. In contrast, 
theory oriented texts in linear algebra uses vector space and its axioms as a 
firm foundation, but the axioms themselves are only motivated informally. 
Situations are further complicated in that vector spaces are used beyond 
geometric situations and in those case the vectors are often deprived of its 
geometric connotation, making it confusing for beginners. (and perhaps equally 
confusing to the experts - which part of vector space do they not understand?)

This chapter seeks to retify these pedagogical problem. As the name suggests, 
Linear Algebra is the algebra of lines. Thus it has its origin in geometry which 
we honour. On the other hand, taking an algebra-first perspective, we are simply 
leveraging opportunistically the nice structures afforded by linearity, 
thus its geometric aspect is de-emphasized. Our overall strategy in developing 
the theory of Linear Algebra is to begin with a geometric focus in this chapter, 
then switch to a purely algebraic paradigm (with geometry serving only as an auxiliary aid) 
afterwards.

Here we will explore the intricate interconnection between geometry and algebra. 
Beyond just motivating vector space through vectors in geometry (as is usually done 
elsewhere), we take on a more rigorous approach - we begin by giving a 
simplified, partial account of synthetic, axiomatic geometry, and then develop 
vectors in that context. We show that vectors arise naturally as a concrete 
representation of translation. We prove that vector addition has the expected 
properties (which is assumed as axiom elsewhere). On the other hand scalar 
multiplication is a trickier question - they are related to another class of 
transform know as Homothety, and are intrinsically bound with a scaling factor, 
which is a number. Here we explore the apparently simple notion of a number line 
- an association between (triplets of) points on line and numbers. We see that 
in case the geometry does not explicitly include an axiom relating numbers and 
lines, homotheties implicitly encode such a relation anyway. We further explore 
the possibility of geometries with non-standard number systems, and in 
particular the example of non-Desarguesian Plane where such association (and 
hence the connection between affine geometry and vector space) breaks down.

In modern geometry, vector space (as part of analytic geometry) is very useful as 
it helps simplify difficult deductions in synthetic geometry - proofs are 
reduced to mere calculations in many case. However we are not actually getting 
any free lunch: this approach works only if we can prove that vector space is in 
fact a model of whatever synthetic geometry we are working in. Unfortunately 
this problem is often swept under the rug and entirely ignored. The theoretical 
developments we've done in the previous sections provide part of that missing 
proof. We assembly the pieces together, and discuss the subtle notion of 
homogenous space in that section. (There is also the other direction of showing 
that synthetic geometry axioms hold inside a vector space, but that is generally 
much easier)

In the last section we review what we have done against the backdrop of 
Erlangen's Philosophy and Programme. We discuss the unifying and organizing 
role classes of transformations play. We end this chapter with a look at 
Projective Geometry, and how paradoxically some of the same developments are 
significantly simpler when redone in there.

\section{Incidence Geometry}

Synthetic Geometry directly axiomatize geometric notion and perform reasoning 
over them, without reference to other entities. There are many different 
geometries, and some notions are not available in all of them. Regardless, 
virtually all geometries at least have the concept of point, line, and 
incidence, hence Incidence Geometry is an universal substrate for them.

\begin{mydef}[Incidence Geometry]
An incidence structure is a tuple $(P, L, I)$ where $P$ is a set of points, $L$ 
is a set of lines, and $I$ is a relation over $P$ and $L$. If $p \sim_I l$ for $p \in P$ 
and $l \in L$, then $p$ lies on $l$, or $l$ passes through $p$. Given such 
structure one may always replace each line with the set of points incident to 
it, and take the new incidence relation to be set membership.

In addition, an incidence relation is a linear space if it satisfies the 
following:
\begin{enumerate}
\item Given any two distinct points, there is exactly one line passing through 
both of them.
\item Any line contains at least two points.
\end{enumerate}
\end{mydef}

A common point of confusing is that some sources treating both synthetic and analytic 
geometry in a liberal manner assume their spaces to be a plane/two dimensional. 
Since we are developing theory with a view to studying vector space, dimension 
is a critical feature. It is possible to do this even in Incidence Geometry.

The following requires knowledge of generation and closure in abstract algebra, 
refer to [sec X].

\begin{mydef}[Linear Set and Dimension]
Given an incidence structure, define an operator $S: \mathcal{P}(P) \rightarrow \mathcal{P}(P)$ as
\[
S(M) = \{ p \in P : \exists l \in L, x \neq y \in P, x \sim_I l \wedge y \sim_l l \wedge p \sim_I l \}
\]  
In words, it includes all points lying on a line from joining any two points in 
$M$.

A subset of points $M$ is a linear set if it is closed under $S$, that is $S(M) = 
M$. In case of Linear space, we can take any two points in $M$ and find the line joining them, 
without leaving $M$. The linear hull of any subset $M$ is the smallest set 
containing $M$ that is a linear set. It may be found by either taking 
intersection over all linear sets containing $M$, or by iterating $S$ on $M$ to 
countable limit.

A basis for a linear set $M$ is a subset $B \subset M$ such that the linear hull 
of $B$ is $M$, and such that there are no set with smaller cardinality 
satisfying the same condition. The dimension of $M$ is then $| B | - 1$.
\end{mydef}

A common technical issue in Synthetic Geometry is degeneracy, when special cases 
arise due to certain elements coinciding/or other cause. For example, zero 
dimensional sets (a single point) poses a problem for our operator $S$. As 
another example, while the linear hull of three points should be a plane, this 
is not the case if they are collinear, i.e. lies on the same line. Degeneracy is 
pervasive, and were we to deal with it every single time it would get unweidly. 
While a rigorous treatment does need to face this issue, here we handwave and 
ignore them by the fuzzy assumption of General Position: that is we always 
assume the elements are chosen in a generic manner that these cases does not 
happen. (It is possible to give a rigorous definition of General Position, which we won't do
here. It is also possible to make the intuition precise that were we to choose 
randomly, degeneracy is indeed an exceptional case that only happens rarely)

\section{Affine Geometry and Translation}

Affine Geometry is centered on just one geometric notion: that of parallel 
lines. Thus unlike the Euclidean Geometry most are familiar with, there is no notion 
of absolute length of a line segment, nor that of angle. However we are able to 
define ratio of length on a line.

A full treatment of Synthetic Affine Geometry is out of scope here. For our 
purpose, it turns out we only need a small number of concepts/propositions 
through which all our derivations rest upon. (This is a common technique to combat 
complexity, to funnel them through a small, well understood interface)

\begin{mydef}[Parallelgram]
Given an affine geometry, and 4 points listed in order $A, B, C, D$, we say that 
they form a parallelgram if, treated as a polygon with line segments $AB$, $BC$, 
$CD$, $DA$, opposite sides are parallel line. That is $AB$ is parallel to $CD$, 
and $BC$ is parallel to $DA$.
\end{mydef}

Notice that by symmetry, shifting our points cyclically preserve such 
properties. (Example: $B, C, D, A$) A diagram is shown below.


We will need the following two propositions provable from the axioms of Affine 
Geometry:

\begin{prop}[Completion of parallelgram]
Given three points in General Position $A, B, C$, there exists a unique point $D$ 
such that $A, B, C, D$ is a parallelgram.
\end{prop}
\begin{proof}
The basic idea is the get the unique line parallel to $BC$ passing through $A$, 
and similarly the unique line parallel to $AB$ passing through $C$. Both lines 
lie inside the linear hull of $\{ A, B, C \}$, which is a plane, hence they 
either intersect at a unqiue point $D$, or they are parallel. But the later case 
is impossible for otherwise by transitivity of parallelity, $AB$ would be 
parallel to $BC$, which implies that $A, B, C$ are collinear.
\end{proof}

\begin{prop}[Transitivity of parallelgram]
Let $A, B, C, D$ and $D, C, E, F$ be parallelgrams. Then $A, B, E, F$ is also a 
parallelgram.
\end{prop}
\begin{proof}
That $AB$ is parallel to $EF$ follows from transitivity of parallel lines. That $AF$ 
is parallel to $BE$ is a special case of the axiom X.
\end{proof}

We begin our study of affine geometry with a basic class of transformation:

\begin{mydef}[Translation]
A translation in an affine geometry with $X$ as the set of point, is a function $T: X \rightarrow X$
such that for any two distinct points $x, y \in X$, $x, T(x), T(y), y$ forms a 
parallelgram.
\end{mydef}

To motivate vector, we consider this property:

\begin{prop}[Classification of translation]
For any pairs of distinct points $x, y \in X$, there exists an unqiue 
translation taking $x$ to $y$. Moreover, another pair of points $x', y' \in X$ 
defines the same translation iff $x, y, y', x'$ is a parallelgram.
\end{prop}
\begin{proof}
Existence: We define a function $T_{x, y}$ that sends any point $z \in X$ to the 
unique point $z'$ completing the parallelgram $x, y, z', z$. This is indeed a 
translation: for any two points $p, q$, $x, y, T_{x, y}(p), p$ and $x, y, T_{x, y}(q), 
q$ are both parallelgram, so by transitivity of parallelgram so is $p, T_{x, y}(p), T_{x, y}(q), 
q$.

Uniqueness: Seeing the requirement for being a translation, and supposing that $T(x) = 
y$, the above construction is the only consistent solution.

As for defining the same translation:
$\Leftarrow$: For any point $z$, the fact that $x, y, T_{x, y}(z), z$ (by construction) 
and $x, y, y', x'$ (by assumption) are parallelgram implies that $x', y', T_{x, y}(z), z$ 
also is one by transitivity. As parallelgram completing the points $x', y', z$ 
is unique, $T_{x', y'}(z) = T_{x, y}(z)$.

$\Rightarrow$: Notice that by degeneracy $T_{x, y}(x) = y$ always. Then denoting 
the common translation by $T$, $T(x) = y$ and $T(x') = y'$, hence $(x, y, y', x') = (x, T(x), T(x'), x')$
is also a parallelgram.
\end{proof}

We now define vectors:

\begin{mydef}[Vectors]
For a given affine geometry $X$, its space of vectors $V_X$ is the set $X \times X / \sim$ 
of pair of points in $X$, quotiented out by the equivalence relation defined as 
follows:
\[
(x_1, y_1) \sim (x_2, y_2) \Leftrightarrow (x_1, y_1, y_2, x_2) \text{ is a parallelgram}
\]
It is in fact an equivalence relation due to the transitivity of parallelgram.
\end{mydef}

From the proposition above, we see that vector is a representation of 
translation. More concretely, a (representative of) vector is a pair of points 
implying a translation from the first point to the second. We graphically 
represent it as follows:


Here the point $a$ is the tail, and point $b$ is the head. The arrow suggests a 
movement from $a$ to $b$, which is indeed what the translation does. In 
geometric context we denote this vector as $\overrightarrow{ab}$, which actually 
means the equivalence class of $(a, b)$. While this notation is very useful in 
geometry, where one works directly with points, it become more of a burden in 
more algebraic usage - the extra strokes for the arrows is a liability and also 
too heavy to the eyes when lots of expressions are involved. For these reasons, 
outside of geometry (and in fact, sometimes even in geometry, if the points are either 
unimportant or well understood) a vector is denoted using letters as variable 
just like an element in a set.

Combining what we've proved so far, there is actually a very tight relations 
between vectors and translations:

\begin{cor}[Correspondence between Vectors and Translation (I)]
Let $T_X$ be the set of all translations in $X$. Then there is a natural 
bijection $\phi$ from $T_X$ to $V_X$, given as follows:
\begin{enumerate}
\item For a given translation $T$, the corresponding vector $\phi(T)$ is $\overrightarrow{x T(x)}$
for any $x \in X$.
\item For a vector with representative $(x, y)$ the corresponding translation is 
$\phi^{-1}(\overrightarrow{xy}) = T_{x, y}$
\end{enumerate}
\end{cor}

It is a tedious but routine exercise to check well-definedness and two-sided 
inverse. We will not cover naturality here, since it requires formulating affine 
transform between affine space.

This is also only the beginning - in the next section we will define vector 
addition and add it to the correspondence between $T_X$ and $V_X$ by definition.

\section{Properties of Vectors}

To further our studies of vector, we will introduce a vector addition operator. 
But first there is an easy way to add a natural operator to $T_X$:

\begin{prop}
The set of translations in $X$, $T_X$, is closed under composition.
\end{prop}
\begin{proof}
Let $U, T$ be two translations. For any two points $x, y \in X$, 
both $x, T(x), T(y), y$ and $T(x), U(T(x)), U(T(y)), T(y)$ 
are parallelgrams, and therefore so is $x, U(T(x)), U(T(y)), y$ by transitivity.
\end{proof}

The reason for such detour is the following punchline/trick:

\begin{mydef}[Vector addition]
Vector addition is the 2-ary operator $+ : V_X \times V_X \rightarrow V_X$ that, 
under the correspondence between $V_X$ and $T_X$, becomes the composition of 
function. Concretely, for $x, y \in V_X$, 
\[
x + y = \phi( \phi^{-1}(x) \circ \phi^{-1}(y) )
\]
\end{mydef}

This may seems awkward and even forced but it is an useful idea that will 
reappear in much more advanced materials in other fields. Namely, in algebra it 
is the structure that matters, and so objects that are isomorphic (especially those 
that are naturally isomorphic) should be thought of as being essentially the 
same thing. Conversely, such isomorphism (or even weaker forms) can be used to 
help us understand new objects in unfamiliar world by transfer of structure, 
properties, insights, etc. In some situations it is even possible to simply 
define such an isomorphism by executive fiat, in leiu of first defining the 
structure on the target object and then proving their equivalence. Two possible 
reasons are: 1) The definition on the other side is much more natural/well 
motivated then anything that can be done directly, or 2) we really have no idea 
how to do it because it is just too complicated, but we do know that 
philosophically they ought to be the same thing.

To further clarify we may consider the following example: there is an 
isomorphism between the group of strictly positive real number under 
multiplication, $(\mathbb{R}^+, \times)$ and the group of real number under 
addition $(\mathbb{R}, +)$, with the mapping given by $\log$ and $\exp$ (Mainly, 
we have that $\log{(xy)} = \log{x} + \log{y}$). Then a sneaky way to define general 
power function is to transfer it into the logarithmic domain $(\mathbb{R}^+, 
\times)$: $\log{x^r} = r \log{x}$, hence taking exponentiation on both side $x^r = \exp{(r \log{x})}$

\begin{cor}[Correspondence between Vectors and Translation (II)]
Let $(T_X, \circ)$ be the (abelian) group of translations in $X$ with group 
operation given by function composition. Let $(V_X, +)$ be the abelian group of 
vectors in $X$, with group operation given by vector addition just defined. Then 
$\phi$ as last corollary is a group isomorphism, that is the following diagram 
commutes:
\end{cor}
\begin{proof}
Follows by definition.
\end{proof}

Having successfully defined the operation, we may now move on to prove their 
properties. First, we need a way to compute it:

\begin{prop}[Rules of vector addition]
If two vectors are aligned with the tail of one coinciding with the head of the 
other, then we have $\overrightarrow{AB} + \overrightarrow{BC} = 
\overrightarrow{AC}$.
\end{prop}
\begin{proof}
By Prop X and Cor Y the sum is a vector corresponding to a translation, hence by 
Cor Z, it suffices to find the image under the resulting translation of any 
point. Here we test against the point $A$. Again, by Cor Z the vector $\overrightarrow{AB}$ 
corresponds to a translation taking $A$ to $B$, and similarly $\overrightarrow{BC}$ 
takes $B$ to $C$. Hence their composition takes $A$ first to $B$, and then to 
$C$, so all in all it takes $A$ to $C$, as expected.
\end{proof}

Thus a general scheme to compute vector addition is as follows: suppose we are 
given vectors $\overrightarrow{AB}$ and $\overrightarrow{CD}$. We move / 
translate the second vector to make its tail aligned with the first's head, 
which can be done by finding the unique parallelgram completing the points $B, C, 
D$. Suppose this point is $E$. Then by the rule above, the sum is simply 
$\overrightarrow{AE}$. For multiple addition we can repeatedly apply this 
procedure.

Finally, we are ready to see its two main properties:

\begin{prop}[Associativity of addition]
For all vectors $\overrightarrow{x}, \overrightarrow{y}, \overrightarrow{z}$, 
\[
( \overrightarrow{x} + \overrightarrow{y} ) + \overrightarrow{z} = \overrightarrow{x} + ( \overrightarrow{y} + \overrightarrow{z} ) 
\]
In particular,
\[
( \overrightarrow{AB} + \overrightarrow{BC} ) + \overrightarrow{CD} = \overrightarrow{AB} + ( \overrightarrow{BC} + \overrightarrow{CD} ) 
= \overrightarrow{AD}
\]
\end{prop}
\begin{proof}
Per discussion above, given the vectors $\overrightarrow{x}, \overrightarrow{y}, \overrightarrow{z}$ 
we may first align them one by one: first align the tail of $\overrightarrow{y}$ 
with the head of $\overrightarrow{x}$, then align the tail of $\overrightarrow{z}$ 
with the head of the moved second vector. Thus we are reduced to the case that $\overrightarrow{x} = 
\overrightarrow{AB}$, $\overrightarrow{y} = \overrightarrow{BC}$, $\overrightarrow{z} = \overrightarrow{CD}$ 
for some points $A, B, C, D$.

The results then follows by applying the rule of addition in different orders:
\begin{eqnarray*}
( \overrightarrow{AB} + \overrightarrow{BC} ) + \overrightarrow{CD}
&=& \overrightarrow{AC} + \overrightarrow{CD} \\
&=& \overrightarrow{AD}
\end{eqnarray*}

\begin{eqnarray*}
\overrightarrow{AB} + ( \overrightarrow{BC} + \overrightarrow{CD} )
&=& \overrightarrow{AB} + \overrightarrow{BD} \\
&=& \overrightarrow{AD}
\end{eqnarray*}
\end{proof}

\begin{prop}[Parallelgram Law and Commutativity of addition]
Suppose $A, B, D, C$ is a parralelgram. Then
\[
\overrightarrow{AB} + \overrightarrow{BD} = \overrightarrow{AC} + \overrightarrow{CD} = \overrightarrow{AD}
\]
and that
\[
\overrightarrow{AB} + \overrightarrow{AC} = \overrightarrow{AC} + \overrightarrow{AB} = \overrightarrow{AD}
\]

As a result, vector addition is commutative: if $\overrightarrow{x}, \overrightarrow{y}$ 
are vectors then $\overrightarrow{x} + \overrightarrow{y} = \overrightarrow{y} + 
\overrightarrow{x}$.
\end{prop}
\begin{proof}
The first set of equalities is just the rule of addition. Then, because $A, B, D, C$ 
is a parallelgram, $\overrightarrow{AC} = \overrightarrow{BC}$ and $\overrightarrow{AB} = 
\overrightarrow{CD}$ by definition of vector as equivalence class, and the observation from 
definition that opposite sides of parallelgram are parallel, whereupon the 
second set of equalities follows by substitution.

Commutativity is a direct consequence of the parallelgram law: this time, align 
the tail of $\overrightarrow{y}$ to the \emph{tail} of $\overrightarrow{x}$ 
instead, and then find the unique point $D$ completing the parallelgram so that
$\overrightarrow{x} = \overrightarrow{AB}$ and $\overrightarrow{y} = 
\overrightarrow{AC}$.
\end{proof}

\section{Homothety, Number Lines, and non-Desarguesian Plane}

\section{Vector space as Model}

\section{Erlangen Programme and Projective Geometry}

\end{document}  
