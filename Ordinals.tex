
\documentclass[11pt]{article}

\usepackage{ifpdf}
\ifpdf 
    \usepackage[pdftex]{graphicx}   % to include graphics
    \pdfcompresslevel=9 
    \usepackage[pdftex,     % sets up hyperref to use pdftex driver
            plainpages=false,   % allows page i and 1 to exist in the same document
            breaklinks=true,    % link texts can be broken at the end of line
            colorlinks=true,
            pdftitle=My Document
            pdfauthor=My Good Self
           ]{hyperref} 
    \usepackage{thumbpdf}
\else 
    \usepackage{graphicx}       % to include graphics
    \usepackage{hyperref}       % to simplify the use of \href
\fi 

\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{amsthm}

\newtheorem{mydef}{Definition}
\newtheorem{prop}{Proposition}
\newtheorem{thm}{Theorem}
\newtheorem{rmk}{Remark}

\newtheorem{cor}{Corollary}

\title{Ordinals}
\author{The Author}
\date{}

\begin{document}
\maketitle

Ordinals and Cardinals are two different way to extend natural numbers to 
infinity and beyond, by imposing two interpretations on them: while Cardinals 
consider number to be a description of size, Ordinals take them to be a measure 
of ranking, or order. Thus the number 4 may mean either something with 4 items, 
or the 4th item in a collection. (The two interpretations are closely related, as we see 
in the mind bending self-referencing definition of Ordinals)

Ordinals classify all possible well orderings. As is usual, we define well 
ordering to be specific structures on a set, and then consider all equivalence 
classes under order isomorphism. Unfortunately, the collection representing an 
equivalent class is too big to be a set; therefore, we cannot develop the theory 
in an ordinary manner and have to use a trick.

\section{Well Ordering}

We begin with the definition:
\begin{mydef}
Let $(P, \leq)$ be a poset. Then it is a Well Ordering if any subsets $X \subset P$ 
contains a minimum element (in the inherited subposet $(X, \leq)$): there is some 
$x \in X$ such that $x \leq y$ for all $y \in X$.
\end{mydef}

Note that we require $X$ to have a minimum and not just a minimal element: 
recall that the former is stronger and implies the later, and that it must be 
unique if it exists (due to antisymmetry axiom of poset) whereas there can be 
more than one minimal elements for general poset. In particular for minimality we 
only require that there is no strictly smaller elements $y < x$.

An infinite descending chain is a countably infinite sequence $x_k$ such that
\begin{equation}
x_1 \geq x_2 \geq \cdots \geq x_n \geq x_{n+1} \geq \cdots
\end{equation}
If there is some $m$ such that $x_{m+k} = x_m$ for all positive integer $k$, 
then the chain is said to stablize. (Inituitively it stops at $x_m$) If 
inequality is strict for all $k$, then it is a strictly descending chain. 
Strictness is sufficient but not necessary for not stablizing.

We have the following mostly obvious equivalence:
\begin{prop}
For a poset $(P, \leq)$ TFAE:
\begin{enumerate}
\item $P$ is a well ordering.
\item $P$ is a total order and all subset $X \subset P$ contains some minimal 
element.
\item $P$ is a total order and all descending chain in $P$ stablize.
\item $P$ is a total order and there is no infinite strict descending chain.
\end{enumerate}
\end{prop}

\begin{proof}
First we note that well ordering implies that the order is total: for any two 
elements $x, y \in P$, take the subset $\{ x, y \}$, which must have a minimum 
element by well ordering, say $x$. Then $y \leq x$.

(1) $\Leftrightarrow$ (2): Minimum element must also be minimal, and for a total 
order the converse is also true (Take $z \in X$ distinct from the chosen minimal 
element $x_0$. If $z < x_0$, this would contradict minimality of $x$, hence by 
trichotomous law $x_0 < z$.)

(1) $\Rightarrow$ (3): Take any infinite descending chain $x_k$. Apply well 
ordering to the subset $\{ x_k \}_{k \in \mathbb{N}}$ to get a minimum element $x_n$ 
for some $n \in \mathbb{N}$. Then $x_n \leq x_{n+k}$ for all $k$, which together 
with $x_n \geq n_{n+k}$ implies that $x_n = x_{n+k}$.

(3) $\Rightarrow$ (4): Follows from remark above: strictness is one particular 
way for a chain to not stablize.

(4) $\Rightarrow$ (1): Suppose towards a contradiction that $P$ is not a well
ordering. Then there is some subset $X$ without minimum element. Thus if we pick 
any element $x_0 \in X$, we can find some $x_1 < x_0$, and we can again find $x_2 < 
x_1$, and so on, because any $x_k$ we are considering is not a minimum. But then 
this gives us an infinite strict descending chain, a contradiction.

\end{proof}

Recall that an order isomorphism between two posets $(P, \leq), (Q, leq)$ is a 
bijective function $f: P \rightarrow Q$ such that for any $x, y \in P$, $x \leq y$ 
iff $f(x) \leq f(y)$. It is obvious that this define an equivalence classes of 
poset (or in our case, well ordering) with the same algebraic structure.

We want to go one step further and define an order to compare two well 
orderings. To this we consider way to embed one well ordering into the other 
from the bottom onwards:

\begin{mydef}
Let $(P, \leq)$ be a well ordering. An initial segment is a subset $Q \subset P$ 
of the form $s_P(\gamma) = \{ x \in P | x < \gamma \}$. (Sometimes more 
inituitively written as $[ 0, \gamma )$, which we will not use to avoid 
ambiguity)
\end{mydef}

\begin{prop}
Any subset $Q \subset P$ of a well ordering is an initial segment iff it is 
downward-closed: that is, if $x \in Q$, then for all $y \in P$ such that $y < 
x$, $y \in Q$ also.
\end{prop}
\begin{proof}
An initial segment is obviously downward closed. For the converse, take the 
complement $P - Q$ and apply well ordering to it to get a minimum $\gamma \in P - 
Q$. I claim that $s_P(\gamma) = Q$. Pick any element $\lambda \in P$ distinct 
from $\gamma$. As well ordering is total, by trichotomous law either $\lambda < \gamma$ 
or $\lambda > \gamma$. In the former case we must have $\lambda \in Q$ since 
otherwise $\lambda \in P - Q$, contradicting $\gamma$ being the minimum. In the 
latter case $\lambda$ cannot be in $Q$. If not, by downward closure property of $Q$ 
$\gamma$ would also be in $Q$, contradicting the statement $\gamma \in P - Q$.
\end{proof}

\section{Ordinals in Axiomatic Set Theory}

We will develop the actual theory formally within the framework of ZFC. Our 
strategy will be to choose a natural representative for each equivalence class: 
this will be sets with certain carefully calibrated properties. The beginning 
phase is particular challenging since many useful tools for reasoning about 
Ordinals are not available yet and we would have to bootstrap.

\subsection{Definitions and Basic Properties}

First a meta definition - as ZFC is a first order theory we won't be able to 
formalize it directly inside the formal machinary, instead we use it as a kind 
of shorthand for expanding out formulas:

Given a definable property $\mathcal{P}$ over a single set (or, a single arity predicate 
if you allow conservative extension), we say that a set $X$ is 
hereditary-$\mathcal{P}$, if we have:
\begin{equation}
\mathcal{P}(X) \wedge (\forall Y \in X, \mathcal{P}(Y) )
\end{equation}

\begin{mydef}
A set $A$ is transitive if for all $B, C$ such that $C \in B \in A$, $C \in A$.
\end{mydef}

\begin{prop}
If a set $X$ is hereditary transitive, then all its member are also hereditary 
transitive, and members of its members, ad infinitum.
\end{prop}
\begin{proof}
It suffices to show that once we have two layers of transitivity, transitivity 
inherents all the way down the chain. In turn, it suffice to show inherentence 
down one layer. That is, if $c \in b \in a$, $a$ is transitive and $b$ ranging 
over elements of $a$ are all transitive, then $c$ ranging over all such chain 
with $a$ fixed are transitive.

To see why this work, look at any chain $\cdots a_n \in a_{n-1} \in 
\cdots \in a_4 \in a_3 \in a_2 \in a_1$, with $a_1$ fixed and the rest ranging 
over all possible elements. Suppose $a_1$ and $a_2$ (free ranging) are all 
transitives. Then $a_3$ (free ranging) is transitive. As $a_2$ and $a_3$ are 
both transitive, then so are $a_4$. Repeating this argument, whenever 
$a_{n-2}, a_{n-1}$ are transitive, so are $a_n$.

Inheritance down one layer is itself relatively trivial: as $a$ is transitive, $c \in b \in a$ 
implies that $c \in a$. But all elements of $a$ are transitive, hence so are 
$c$.
\end{proof}

There is also a useful general property of set we will repeatedly rely on:
\begin{prop}
There is no finite cycle in any directed graph formed by taking set membership 
as a directed edge. That is, for any finite chain $a_n \in a_{n-1} \in \cdots \in a_2 \in 
a_1$, we must not have $a_n = a_1$.
\end{prop}
\begin{proof}
Just apply the well founded axioms.
\end{proof}

We can now undertake to define ordinals:
\begin{thm}[Ordinal as a class]
Given a set $X$, TFAE:
\begin{enumerate}
\item $X$ is hereditary transitive.
\item $X$ is transitive, and $(X, \in)$ defines a total order.
\end{enumerate}

We define a predicate $\text{Ord}$ by the equivalent conditions above, which 
then define a (proper) class.
\end{thm}
\begin{proof}
(2) $\Rightarrow$ (1): Consider a chain $c \in b \in a \in X$. As $X$ is 
transitive, $a, b, c$ are all in $X$. Then as $(X, \in)$ is a total order, we 
must have either $a \in c$ or $c \in a$ or $a = c$. The last case gives us a 
cycle $a \in b \in a$ which is forbidden by prop 4. The first case gives us a 
slightly longer cycle $a \in c \in b \in a$ which is again forbidden. Hence $c \in 
a$.

(1) $\Rightarrow$ (2): Suppose towards a contradiction that $(X, \in)$ is not 
total. Then we consider a minimal counterexample by first forming the set
\begin{equation}
M_1 = \{ x \in X | \exists y \in X, x \text{ incomparable to } y \}
\end{equation}
By weak well ordering of $(X, \in)$, there must exists a minimal $p \in M_1$. 
Now construct
\begin{equation}
M_2 = \{ x \in X | x \text{ incomparable to } p \}
\end{equation}
Which is nonempty as $p \in M_1$. Again, take a minimal $q \in M_2$.
Then $p, q$ forms a minimal pair that is incomparable.

I now claim that in spite of construction, $p = q$ in fact which is a 
contradiction. The only way out is that the set $M_1$ is empty; that is, $(X, \in)$ 
is total.

Consider any $a \in p \in X$. By transitivity of $X$, $a \in X$ 
also. Now $a \in p$ means precisely that $a < p$, hence by minimality of $p$, $a$ 
is comparable to all elements in $X$. We then must have $a \in q$ for the other two 
cases both lead to contradiction: $a = q \Rightarrow q \in p$, while $q \in a \in p$ 
implies by transitivity of the poset (which follows from the hereditary part of $X$ being 
a transitive set) that $q \in p$ also.

In a similar fashion we may consider any $b \in q$; we must have $b \in p$ also. 
Indeed, $b$ is also in $X$ and with $b < q$ and the minimality of $q$ with 
respect to comparability against $p$, $b$ must be comparable with $p$. $b = p$ 
implies that $p \in q$, while $p \in b \in q$ also implies $p \in q$.

What we have shown above is that for any $e$, $e \in p \Leftrightarrow e \in q$, 
so $p = q$ as promised.
\end{proof}

As an early attempt to bridge the two development of the same theory, we show 
that all ordinals, as cannonical representatives, are themselves honest well 
ordering under the standard $\in$ relation:

\begin{prop}
For any Ordinals $X$, $(X, \in)$ defines a well ordering.
\end{prop}
\begin{proof}
We check each of the property:
\begin{description}
\item[Poset] Antisymmetry law is true as otherwise we would have $a \in b \in 
a$. Transitivity follows from (2) $\Rightarrow$ (1) part of theorem 1.
\item[Total order] Shown by theorem 1.
\item[Well ordered] If not, we would have an infinite, strict descending chain 
$\cdots \in x_n \in \cdots \in x_2 \in x_1 \in X$, which contradict well 
foundedness.
\end{description}
\end{proof}

\begin{thm}[Recursive characterisation of Ordinals (Internal version)]
Let $A, B$ be ordinals. Then $A \in B$ iff $A$ is a proper subset of $B$. 
Moreover, if such condition is satisfied, then $A$ is in fact an initial segment 
of $B$, and we have the (internal) recursive formula
\begin{equation}
A = s_B(A)
\end{equation}
Conversely, all initial segments of $B$ arise in such manner, i.e. as an 
Ordinal itself and hence satisfy the conditions above.
\end{thm}
\begin{proof}
First, suppose $A \in B$. Then by definition of Ordinals, $B$ is transitive. 
Hence for all $X \in A \in B$, $X \in B$ also, so $A$ is a subset of $B$. The 
subset relation is proper since otherwise we would have $B \in B$.

For the other direction, note that $A$ (as a subset of the well ordering $(B, \in)$) 
is downward closed: for any $C \in B$, $C < A$ implies $C \in A$ just by 
definition. Hence it is an initial segment and we just need to solve the 
equation so to speak. Henceforth let 
\begin{equation}
A = s_B(\gamma) = \{ x \in B | x < \gamma \}
\end{equation}
for some $\gamma \in B$.
By set equality, we have $y \in A \Leftrightarrow (y \in B \wedge y \in 
\gamma)$. However, since $A$ is a subset of $B$, the left hand side subsumes the 
condition $y \in B$ on right hand side. On the other hand, $y \in \gamma$ 
already subsume $y \in B$ as required $\gamma \in B$ and $B$ is transitive. To 
conclude, the condition $y \in B$ is redundant and can be safely removed, upon 
which $A = \gamma$ by extensionality axiom of set theory. Then we're done with 
this part as $A = \gamma \in B$.

Finally, to see that any downward closed subset $P$ of $B$ must be an Ordinal, use 
definition (1). The hereditary part is trivially satisfied as by definition (1) 
on $B$ its elements are all transitive. So it remains to check that the subset 
itself is transitive. But given $r \in q \in P \subset B$, this implies $r \in q \in 
B$, and transitivity of $B$ gives $r \in B$ also. Now definition of the order on $B$ 
gives $r \in q \Rightarrow r < q$. By downward closure property, $r \in P$, as 
desired.
\end{proof}

Before we finish up the main theorem of this section, we need just one more 
simple property:

\begin{prop}
Ordinals are closed under set intersection for arbitrary family. That is, given $\{ A_i \}_{i \in 
I}$, $A_i \in \textbf{Ord}$ for all $i \in I$, we have $\cap_{i \in I} A_i \in \textbf{Ord}$ 
also.
\end{prop}
\begin{proof}
Use definition (1) of ordinal and check directly. Similar to proofs of last 
theorem, the hereditary part is trivial since the elements of $A_i$ are all 
transitive. The intersection is itself a transitive set: if $y \in x \in \cap_{i \in I} 
A_i$, we would have $y \in x \in A_i$ for all $i$, so $y \in A_i$ always by 
transitivity of each $A_i$, and then $y \in \cap_{i \in I} A_i$.
\end{proof}

\begin{thm}[Trichotomous Law of Ordinals]
Define an order on the class of all Ordinals by the subset relation. Then it is 
a total order: for any two Ordinals $A, B$ at least one of the following must be true:
\begin{equation}
A > B, A = B, A < B
\end{equation}
\end{thm}
\begin{proof}
Consider the set $A \cap B$. It is an Ordinal since they are closed under 
intersection. Moreover it is a subset of both $A$ and $B$, hence $A \cap B \leq A$ 
and $A \cap B \leq B$, with inequality being strict iff the subset is proper. I 
claim that at least one of them is actually an equality. Result will follow 
since if say $A \cap B = A$, then the other inequality gives $A = A \cap B < B$ 
- the remaining one must be strict since otherwise $A = A \cap B = B$.

Suppose to the contrary that both inequalities are strict. Then by the internal 
characterisation of Ordinals, we now have
\begin{eqnarray}
A \cap B \in A \\
A \cap B \in B
\end{eqnarray}
But any set $X$ that is element of both $A$ and $B$ would be an element of $A \cap B$ 
also, so we now get $A \cap B \in A \cap B$, a contradiction!
\end{proof}

\begin{cor}
The class $\textbf{Ord}$ with an order given by set inclusion is a well 
ordering.
\end{cor}
\begin{proof}
It is a poset by inheriting the standard poset structure of set inclusion. 
Totality is shown by the trichotomous law just proved. Arbitrary subclass of it has a 
minimum element: just take the intersection as in the standard poset of set 
inclusion.
\end{proof}

\begin{thm}{Recusive characterisation of Ordinals (External version)}
All ordinal contains precisely the ordinals that are strictly smaller than it 
under the well order defined above. Informally, $X = [ 0, X )$.
\end{thm}
\begin{proof}
First, that elements of ordinals are all ordinals follows from definition (1) 
and prop X. Then, result is just a restatement of the internal characterisation 
that $Y \in X \Leftrightarrow Y < X$.
\end{proof}

\subsection{Types of Ordinals}

\subsection{Transfinite Induction}

\end{document}
