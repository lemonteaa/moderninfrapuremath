
\documentclass[11pt]{article}

\usepackage{ifpdf}
\ifpdf 
    \usepackage[pdftex]{graphicx}   % to include graphics
    \pdfcompresslevel=9 
    \usepackage[pdftex,     % sets up hyperref to use pdftex driver
            plainpages=false,   % allows page i and 1 to exist in the same document
            breaklinks=true,    % link texts can be broken at the end of line
            colorlinks=true,
            pdftitle=My Document
            pdfauthor=My Good Self
           ]{hyperref} 
    \usepackage{thumbpdf}
\else 
    \usepackage{graphicx}       % to include graphics
    \usepackage{hyperref}       % to simplify the use of \href
\fi 

\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{amsthm}

\newtheorem{mydef}{Definition}
\newtheorem{prop}{Proposition}
\newtheorem{thm}{Theorem}
\newtheorem{lem}{Lemma}
\newtheorem{rmk}{Remark}

\usepackage{mdframed}

\title{Classification of Semisimple Lie Algebra I}
\author{The Author}
\date{}

\begin{document}
\maketitle

Note: Throughout this chapter all Lie Algebras mentioned are assumed to be complex and 
finite dimensional. It is also assumed to be semisimple except in Section 2.

\section{Overview of strategy}

\section{General Theory of Cartan Subalgebra}

The nicest kind of (finite dimensional) linear operator is diagonalizable. Thus 
for a linear representation of Lie Algebra the easiest case is when the elements 
are simultaneously diagonalizable, which we recall from Linear Algebra requires 
elements to commute. In such case it is also elementary to show that the 
resulting eigenvalues are then linear in $\mathfrak{g}$. In general we cannot 
expect such niceness, however we can still find a subalgebra that behaves like 
this. What we aim to do is the find the maximal such algebra (which is abelian as 
elements commutes) for the adjoint representation of the semisimple Lie Algebra 
being classified, and then to go on from there.

Such subalgebras are called Cartan Subalgebra. Unfortunately it is not at all 
obvious that it is nontrivial or that it is unique (it is not, but at least do we have 
weaker uniquness property?)

It turns out to be possible to develop a weak form of such theory without the 
semisimple assumption - some author find it more efficient to develop this 
generalized theory first and then apply it to the special case at hand. The 
tradeoff is that the general definition appear opaque at first. Nonetheless we 
will still show that we can have simultaneous trianglization in general.

\subsection{Definition, Existence and Uniqueness}

\begin{mydef}[Cartan Subalgebra]
A lie subalgebra $\mathfrak{h} \subset \mathfrak{g}$ is a Cartan Subalgebra if 
it is nilpotent and self-normalizing (i.e. $[ x, y ] \in \mathfrak{h}$ for all $x \in \mathfrak{h}$ 
implies that $y \in \mathfrak{h}$ also).
\end{mydef}

Since we are interested in decomposition of linear operator, we may as well use 
the heavy-weight of Jordan Decomposition from the get-go: then we only need 
worry about the lie-algebraic aspect of simultaneaty. So let's fix some notation 
first.

For $X \in \mathfrak{g}$, write the Jordan Decomposition of the adjoint $\text{ad} X: \mathfrak{g} 
\rightarrow \mathfrak{g}$ as $\mathfrak{g} = \oplus_\lambda \mathfrak{g}_\lambda(X)$ 
where $\lambda$ runs through generalized eigenvalues of $X$ and $\mathfrak{g}_\lambda(X)$ 
denotes the generalized eigenspace $\cup_{i = 1}^\infty \text{ker} (\text{ad} X - 
\lambda)^i$. We allows $\lambda$ to run through all $\mathbb{C}$: in case it is 
not a generalized eigenvalue we just let $\mathfrak{g}_\lambda(X) = \{ 0 \}$.

Quick reminder: The action of the adjoint is denoted using a dot $\text{ad} X \cdot v = [X, v]$ 
per definition. Thus when we compose $\text{ad} X$ and $\text{ad} Y$ say we have 
$\text{ad} Y \cdot \text{ad} X \cdot v = [ Y, [X, v]]$.

The following formula is very powerful and will be used almost every line:

\begin{prop}[Basic Formula]
\[
[ \mathfrak{g}_\lambda(X), \mathfrak{g}_\mu(X) ] \subset \mathfrak{g}_{\lambda + \mu}(X)
\]
\end{prop}
\begin{proof}
We first show this identity:
\[
(\text{ad} X - (\lambda + \mu)) \cdot [Y, Z] = [ (\text{ad} X - \lambda) \cdot Y, Z] 
+ [ Y, (\text{ad} X - \mu) \cdot Z]
\]
This is a non-commutative analog of the product rule for differentiation.

To prove this, first extract the linear term $ - (\lambda + \mu) [Y, Z] $ from 
both sign by distributivity and linearity. We are reduced to showing:
\begin{eqnarray*}
\text{ad} X \cdot [Y, Z] &=& [ \text{ad} X \cdot Y, Z] + [Y, \text{ad} X \cdot Z] 
\\
 \left[ X, [Y, Z ] ] &=& [ [X, Y], Z] + [Y, [X, Z]]
\end{eqnarray*}
Which is just the Jacobi identity.

With this identity, we can then iterate it - continuing the analogue we are 
doing the binomial theorem for differentiation. Thus $(\text{ad} X - (\lambda + \mu))^k [Y, Z]$ 
will be a sum of terms of the form $[(\text{ad} X - \lambda)^p \cdot Y, (\text{ad} X - \mu)^q \cdot 
Z]$. Since we are working in finite dimension, by taking a sufficiently high 
power, by pigeonhole principle at least one of $Y$ or $Z$ will get enough power 
to be killed in all terms. Finish it up by seeing $Y \in 
\mathfrak{g}_\lambda(X)$, $Z \in \mathfrak{g}_\mu(X)$.
\end{proof}

This already hints at something interesting: that if we take $\lambda = 0$, then 
all $\mathfrak{g}_\mu(X)$ will be invariant under the action of 
$\mathfrak{g}_0(X)$. Thus the zero generalized eigenspace is special. This gives 
us a way to construct CSA (Cartan Subalgebra), as we show below. This is also 
the beginning of a long series of things that are special for it.

\begin{mydef}[Regular elements]
An element $X$ is regular if the algebraic multiplicity of 0 as generalized eigenvalue 
is as small as possible (compared to other elements in $\mathfrak{g}$). If not, 
then it is a singular element.
\end{mydef}

\begin{thm}[Existence and Uniqueness of CSA]
\begin{enumerate}
\item The set of regular elements in $\mathfrak{g}$ is a Zaraski-open set, and 
invariant under automorphism of $\mathfrak{g}$.
\item For all regular elements $X$, $\mathfrak{g}_0(X)$ is a CSA.
\item All CSA arise as $\mathfrak{g}_0(X)$ for some regular $X$; hence CSA is 
unqiue up to automorphism.
\end{enumerate}
\end{thm}
\begin{proof}
2. The basic formula means that $\mathfrak{g}_0(X)$ is a Lie Subalgebra.

Nilpotence: By Engel's theorem, it suffices to show that $\text{ad} Y$ is 
nilpotent for all $Y \in \mathfrak{g}_0(X)$. Let's start by looking at $X \in \mathfrak{g}_0(X)$ 
first. (We have $0 = [X, X] = \text{ad}X \cdot X$)

Per remark above all elements in $\mathfrak{g}_0(X)$ act invariantly on each $\mathfrak{g}_\lambda(X)$, so 
let's analyze it on each such subspace. For $\lambda \neq 0$ the operator $\text{ad} X$ 
has representation as $\lambda I + N$ ($N$ nilpotent) and so is upper-triangular with 
no zero's on diagonal, and hence invertible. By continuity we can get a 
neighbour of $X$ so that they are all invertible on all subspaces except possibly $\lambda = 
0$. But $X$ is regular, so for all $Y$ in this neighbourhood, their algebraic 
multiplicity of $0$ in $\mathfrak{g}_0(X)$ (which is their only chance as they are 
invertible elsewhere) have to match that of $X$, which is full dimensional ($= \text{dim} 
\mathfrak{g}_0(X)$). This means that their diagonals there are all zero, that is 
they are just nilpotent there - we've done it locally.

Here's the punchline: We can extend this to globally by algebraic continuation, 
because nilpotency is a Zariski-closed condition! (i.e. Satisfying some polynomial 
equation)

Self-normalizing: A direct check works. If $[Z, Y] \in \mathfrak{g}_0(X)$ for 
all $Z \in \mathfrak{g}_0(X)$, then we pick $Z = X$, and then $\text{ad} X \cdot Y = [X, Y] \in 
\mathfrak{g}_0(X)$. Per remark above again, decompose $Y$ with respect to $\{ \mathfrak{g}_\lambda(X) 
\}$, then the statement means that applying $\text{ad} X$ kills all components 
where $\lambda \neq 0$. But it is invertible on all those subspaces so taking 
inverse on them each, $Y$ has no components in $\lambda \neq 0$ either, or $Y \in \mathfrak{g}_0(X)$ 
in other words.
\end{proof}

\subsection{Root Space Decomposition}

From this section onward we denote a CSA by $\mathfrak{h}$.

We aim to show that generally, we can do a simultaneous triangularization. Since 
we already have the Jordan Decomposition for each operator individually with 
invariant subspaces/blocks (and they are upper-triangular: the nilpotent part 
occupy the upper right part of the matrix), an obvious idea is to somehow 
intersects these blocks. This works as expected due to the two defining 
properties of CSA.

\begin{mydef}[Root Space]
For each linear functional $\lambda \in \mathfrak{h}^*$ let $\mathfrak{g}_\lambda$ 
be the intersection of the generalized eigenspace of $\text{ad} H$ with 
generalized eigenvalue $\lambda(H)$ over all $H \in \mathfrak{h}$.

The set of roots $\Delta$ is then the set of $\lambda \neq 0$ such that 
$\mathfrak{g}_\lambda$ is nontrivial. We denote roots by $\alpha, \beta, \gamma, \cdots$ 
by convention; for a root $\mathfrak{g}_\alpha$ is the associated root space. We 
also let $\Delta_0 = \Delta \cup 0$.
\end{mydef}

TODO: Some remarks on the specialness of 0.

\begin{thm}[Root Space Decomposition]
Given a CSA, there exists a finite set of roots $\Delta$ such that:
\begin{enumerate}
\item $\mathfrak{g} = \mathfrak{h} \oplus \left( \oplus_{\alpha \in \Delta} \mathfrak{g}_\alpha \right)$
\item All $\text{ad} H$ for $H \in \mathfrak{h}$ acts invariantly on the decomposition 
in 1.
\item For each root there is a simultaneous triangularization of all $\text{ad} H \in \mathfrak{h}$ 
with diagonal entries given by $\alpha(H)$ in the associated root space.
\end{enumerate}
\end{thm}
\begin{proof}

First, we need to describe the process of obtaining those simultaneous invariant 
subspaces more precisely. We borrow the notion / spirit of refinement from Jordan-Holder 
Decomposition. Given two direct sum decomposition of the vector space 
$\mathfrak{g}$, say $\oplus_{i \in I} U_i$ and $\oplus_{j \in J} W_j$, we say 
that the later is a refinement of the former, if each $W_j$ is a subapce of some 
$U_i$ and for each $U_i$, there is a subset $\sigma(i) \subset J$ such that $\{ \sigma(i) \}_{i \in I}$ 
is a partition of $J$, and $U_i = \oplus_{j \in \sigma(i)} W_j$. Intuitively 
this means that we do not change the existing subspaces other than by possibly 
breaking it down further into direct sum of smaller subspaces. If the vector 
space in question is finite dimensional, it follows that we can only perform at 
most finitely many nontrivial refinement - for they will always decrease the 
dimension of some subspace, and no more refinement is possible if all subspaces 
are of dimension one.

We will prove 1 and 2 by performing a suitable sequence of refinement of the 
direct sum decomposition of $\mathfrak{g}$: we claim that the sequence we are 
going to construct will satisfy the following invariance throughout:
\begin{itemize}
\item At any stage, all subspace must be of the form $\mathfrak{g}_{\lambda_1} (H_1) \cap 
\mathfrak{g}_{\lambda_2} (H_2) \cap \cdots \cap \mathfrak{g}_{\lambda_k} (H_k)$ 
for $H_1, H2, \cdots, H_k \in \mathfrak{h}$.
\item At any stage, all subspaces are invariant under $\text{ad} H$ for all $H \in 
\mathfrak{h}$.
\end{itemize}
We denote the summands at stage $n$ as $\mathfrak{g}_i^n$.

This is where the first time where property of CSA is relied upon: Because a CSA 
is a nilpotent Lie Algebra, $\mathfrak{h} \subset \mathfrak{g}_0(H)$ for all $H \in 
\mathfrak{h}$. (We will use the other property when we look at the special place 
of $\mathfrak{h}$ in the decomposition) The proof is just a direct check: pick $H' \in 
\mathfrak{h}$, expand $(\text{ad} H)^r \cdot H' = [ H, [ H, [ \cdots [ H, H']]]] = 0$ 
for sufficiently large $r$ due to nilpotence.

With this proposition in hands, we can see that the second invariance above is a 
direct consequence of the first: Given $h \in \mathfrak{h}$, we invoke the 
proposition with $\mathfrak{h} \subset \mathfrak{g}_0(H_i)$, i.e. a different $H_i$ 
each times. So 
\[
\text{ad} h \cdot \mathfrak{g}_{\lambda_i}(H_i) = [h, \mathfrak{g}_{\lambda_i}(H_i)]
\subset [ \mathfrak{g}_0(H_i), \mathfrak{g}_{\lambda_i}(H_i) ]
\subset \mathfrak{g}_{\lambda_i}(H_i)
\]
Since these conditions are and'ed, we can take intersection on the right hand 
side to get the result.

Now for the actual process. Start by taking $\oplus_\lambda \mathfrak{g}_\lambda(H)$ 
for any $H \in \mathfrak{h}$. At any stage, we seek a refinement into the next stage by 
searching for any $H^* \in \mathfrak{h}$ such that the set 
$\mathfrak{g}_\mu(H^*)$, when intersected with the current decomposition 
$\mathfrak{g}_i^n$, give off new subspaces. More precisely $\{ \mathfrak{g}_i^n \cap 
\mathfrak{g}_j(H^*) \}_{i, j}$ will be a (possibly trivial) refinement: By 
tacitly dropping these terms that turns out to be trivial, it is easy to check 
that $\mathfrak{g}_i^n = \oplus_j \mathfrak{g}_i^n \cap \mathfrak{g}_j(H^*)$. (Note that it is 
certainly possible for a block $\mathfrak{g}_j(H^*)$ to cut across $\mathfrak{g}_i^n$ 
even though they are all $\text{ad} \mathfrak{h}$ invariant) We use this as the 
next stage $\mathfrak{g}_i^{n+1}$ as long as we find any $H^*$ that gives a 
nontrivial refinement, and we terminate iff no such $H^*$ can be found. Per 
discussion above due to dimensional consideration this will always happen after at 
worst finitely many steps.

The interpretation for this construction is that we split a given block / 
subspace $W$ whenever there is any $H^*$ for which more than one distinct Jordan 
block appears within $W$. When we terminate, the condition means that on any 
block, all $\text{ad} H$ will have only one generalized eigenvalue.

We now skip ahead to prove 3 first. From the just proved 2, and the single 
generalized eigenvalue property we just mentioned, we can apply Lie's Theorem to 
the action of the nilpotent, and hence solvable, Lie Algebra $\mathfrak{h}$ on 
the vector space $\mathfrak{g}_i^N$. This gives us a simultaneous 
triangularization. Because there is only a single generalized (and hence ordinary) 
eigenvalue for each $\text{ad} H$, by looking at the corner entry in the 
triangular matrix (or using the weaker form of Lie's Theorem that produces just a joint 
ordinary eigenvector), we see that this single (generalized) eigenvalue (which is also the 
common diagonal entryies in the matrix representation of $\text{ad} H$) is a 
linear function of $H$ in $\mathfrak{h}$.

It remains to prove that the end product of our refinement procedure admits a 
description as in 1. There are three parts to it:
\begin{enumerate}
\item That given a constructed subspace $\mathfrak{g}_\alpha$ with the 
constructed root by the argument in 3, and being a finite intersection of 
generalized eigenspace of different $H$s, it is actually equal to the infinite 
intersection of generalized eigenspaces of $\alpha$ over all $H$.
\item That $\mathfrak{g}_0 = \mathfrak{h}$, and it always appear in the end 
product.
\item That given our decomposition, no other roots exists.
\end{enumerate}
We note that it is in 2 here that we need the second property (self-normalizing) 
of CSA.

TODO: prove 1 + 3 (or others)

Given 1 and 3, we finally prove 2. First $\mathfrak{g}_0 \supset \mathfrak{h}$: 
recall that by the consequence of nilpotence $\mathfrak{h} \subset \mathfrak{g}_0(H)$ 
for all $H \in \mathfrak{h}$; just intersect over all such $H$. For the converse 
direction, suppose that we have strict inequality as vector space, then take the 
quotient vector space $\mathfrak{g}_0 / \mathfrak{h}$. The adjoint action 
descend to this new space since if we shift the representative by say $h'$, the 
result also shift by $\text{ad} h \cdot h' = [h, h'] \in \mathfrak{h}$. Now 
observe that due to nilpotency, even for this different Lie Algebra Representation the only 
possible ordinary eigenvalue is 0: for if not by iterating the operator we see 
that the operator will not be nilpotent, a contradiction. Now apply Lie's 
Theorem again to get a nonzero joint ordinary eigenvector. Lift it by 
representative to $Y \in \mathfrak{g}_0$: we have $Y \notin \mathfrak{h}$ as it 
is nonzero in the quotient. Because the eigenvalue is 0, lifting yet again, $[H, Y] \in \mathfrak{h}$ 
for all $H \in \mathfrak{h}$. But CSA is self-normalizing, hence $Y \in 
\mathfrak{h}$, a contradiction!


\end{proof}

\subsection{Other Properties}

These will be useful in the semisimple case:

\begin{prop}[Properties of Root Space Decomposition]
Recall $K( X, Y) = \text{Tr}(\text{ad} X \cdot \text{ad} Y)$ is the killing 
form.
\begin{enumerate}
\item $[ \mathfrak{g}_\lambda, \mathfrak{g}_\mu ] \subset \mathfrak{g}_{\lambda + \mu}$ 
for all roots $\lambda, \mu$
\item Whenever $\lambda + \mu \neq 0$, $\mathfrak{g}_\lambda \perp \mathfrak{g}_\mu$ 
under the killing form.
\item We have the explicit formula when restricted to $\mathfrak{h}$:
\[
K(H, H') = \sum_{\alpha \in \Delta} n_\alpha \cdot \alpha(H) \cdot \alpha(H')
\]
\end{enumerate}
\end{prop}

\begin{proof}
\begin{enumerate}
\item Just take intersection on the basic formula, similar to what we did when 
proving the root space decomposition.
\item This is the first instance (outside of the theory of $\mathfrak{sl}_2$) of 
the eigenvalue shifting technique. By 1 above, for all $\nu$ a root we have $[ 
\mathfrak{g}_\lambda , [ \mathfrak{g}_\mu, \mathfrak{g}_\nu ]] \subset 
\mathfrak{g}_{(\lambda + \mu) + \nu}$. That is the composite operator $\text{ad} X \cdot \text{ad} 
Y$, where $X \in \mathfrak{g}_\lambda$ and $Y \in \mathfrak{g}_\mu$, sends a 
generalized eigenvector into a generalized eigenspace with the generalized 
eigenvalue shifted upward by $\lambda + \mu$. If this number is nonzero, by 
repeatedly applying it it will eventually get sent beyond all generalized 
eigenspace of $\mathfrak{g}$ and hence to 0. Thus the composite operator is 
nilpotent, hence admit an upper triangular matrix form and so have trace zero.
\item Just use the matrix representation of the root space decomposition, and 
verify by computation that 
\[
(\lambda I + N_1) (\mu I + N_2) = \lambda \mu I + (\mu N_1 + \lambda N_2 + N_1 
N_2)
\]
Note that the last three terms are all nilpotent, hence the product has a 
diagonal with value all equal to $\alpha(H) \alpha(H')$, and the diagonal is $n_\alpha$ 
long. Sum across all blocks to get the formula.
\end{enumerate}
\end{proof}

\begin{rmk}
Note that 2 above implies in particular that $\mathfrak{h} = \mathfrak{g}_0$ is 
perpendicular to all other root spaces, and that the killing form is trivial on 
all nonzero root's root spaces. In subsequent sections, we will use this 
property and the non-degeneracy of the Killing Form repeatedly.

Also, 3 is similar to the Character Formula.
\end{rmk}

\section{Cartan Subalgebra in a Semisimple Lie Algebra}

The most important consequence of semisimplicity is that now the Killing Form is 
non-degenerate. We first prove a short lemma:

\begin{lem}
If all roots vanish on some $H \in \mathfrak{h}$, then $H = 0$.
\end{lem}
\begin{proof}
Using the explicit formula for killing form, we see that $H$ is perpendicular to 
all elements in $\mathfrak{h}$. But by the remarks it is also perpendicular to 
all other root spaces, and hence it is perpendicular to all vectors in 
$\mathfrak{g}$. By non-degeneracy of the Killing form this implies $H = 0$.
\end{proof}

Our first order of business is to get back the nice picture we have in mind: 
that the CSA is the maximal abelian subalgebra over which the adjoint 
representation admits simultaneous diagonalization.

\begin{prop}
Given CSA in the general theory:
\begin{enumerate}
\item $\mathfrak{h}$ is abelian.
\item The Killing Form is non-degenerate also on $\mathfrak{h}$ and on the paired 
subspaces $\mathfrak{g}_\alpha$ vs $\mathfrak{g}_{-\alpha}$ for all nontrivial 
roots $\alpha$.
\item There is a simultaneous diagonalization of all $H \in \mathfrak{h}$; 
Moreover we have the formula
\[
\text{ad} H \cdot X = \alpha(H) X
\]
Whenever $X \in \mathfrak{g}_\alpha$.
\end{enumerate}
\end{prop}
\begin{proof}
\begin{enumerate}
\item Consider the derived subspace $[ \mathfrak{h}, \mathfrak{h} ]$ measuring 
non-abelianness. Pick some $[H_1, H_2]$ in it, use the triangular form, and 
perform calculation on multiplying triangular matrices (similar to 3 of prop X). 
Now the diagonal is $\alpha(H_1) \alpha(H_2) - \alpha(H_2) \alpha(H_1) = 0$, so $\alpha( [H_1, H_2 ]) = 
0$ for all $\alpha$. Result follows from the lemma.
\item Per remarks, the Killing form is already trivial when evaluated on $x \in \mathfrak{h}$ 
versus any other root spaces. If $x$ is also perpendicular against all of 
$\mathfrak{h}$, then it is perpendicular to the whole space, so non-degeneracy of 
the form on $\mathfrak{g}$ implies $x = 0$. An exactly analogous argument 
applied to $x \in \mathfrak{g}_\alpha$ against all $\mathfrak{g}_\mu$ (for $\mu 
\neq -\lambda$ so that property 2 applies) and then against $\mathfrak{g}_{-\alpha}$ 
gives the second result.
\end{enumerate}
\end{proof}

\section{Root System}

(Not sure?) Historically, extensive calculations of the set of roots for various 
known classes of Lie Algebra (which is not hard to find since they are the 
linearization of Lie Group, which in turn is motivated from continuous symmetry 
in geometry and intuition easily produces the various classes) are performed and 
the results visually examined. It is noticed that they exhibit nice patterns 
that suggests connection with lattices in Euclidean space. Thus the set of roots 
eventually become the key invariant for classifying semisimple Lie Algebra.

In modern treatment, root system is given an axiomatic charaterization, and then 
one shows that the set of root derived from any semisimple Lie Algebra always 
satisfy these axioms. In the next chapter we will show how there is only a small 
number of possible families of root system (alongside a finite number of exceptional 
cases). In the chapter after that this programme is finished off by proving 
existence and uniqueness of semisimple Lie Algebras with each possible root 
system.

\subsection{Elementary Property}

The first two axioms follows directly from what we know so far using only 
elementary arguments:

\begin{prop}[Axiom 1 and 2 of root system]
\begin{enumerate}
\item The set of roots $\Delta$ span the dual space $\mathfrak{h}^*$.
\item The set of roots is closed under reflection through origin: $-\Delta \subset 
\Delta$.
\end{enumerate}
\end{prop}
\begin{proof}
\begin{enumerate}
\item Extrinsic argument: Pick any basis of $\mathfrak{h}$, say $h_1, \cdots, 
h_m$. To match an unknown linear functional it suffices to match its value on 
this basis by linearity. Thus we are reduced to solving this system of linear 
equations:

\begin{equation}
\begin{pmatrix}
\alpha(h_1) & \beta(h_1) & \cdots \\
\alpha(h_2) & \beta(h_2) & \cdots \\
\vdots & \vdots & \\
\alpha(h_m) & \beta(h_m) & \cdots \\
\end{pmatrix}
\begin{pmatrix}
c_1 \\ c_2 \\ \vdots
\end{pmatrix}
=
\begin{pmatrix}
v_1 \\ v_2 \\ \vdots \\ v_m
\end{pmatrix}
\end{equation}

Then the lemma implies that its rows are all linearly independent, hence its 
row-rank is full ($=m$). By the fundamental theorem of linear algebra, thus the 
column rank is also full, so this system always have solutions.

\item By the non-degeneracy of the Killing form over the pairs $\mathfrak{g}_\alpha$ 
vs $\mathfrak{g}_{-\alpha}$, if $\mathfrak{g}_\alpha$ is nontrivial, so must 
$\mathfrak{g}_{-\alpha}$, or we could repeat the same argument and violate the 
overall non-degeneracy of the Killing Form over $\mathfrak{g}$.
\end{enumerate}
\end{proof}

\subsection{Coroot construction and finding SL2 subalgebra}

This is arguably one of the two key steps. Suppose we are given any nontrivial 
$\mathfrak{g}_\alpha$. Pick a nonzero $X$ in it. By non-degeneracy of the 
Killing Form over the pairs $\mathfrak{g}_\alpha$ vs $\mathfrak{g}_{-\alpha}$ 
and axiom 2 of root system we just proved, we can find some $Y \in \mathfrak{g}_{-\alpha}$ 
so that $K(X, Y) \neq 0$.

Next we have a fundamental formula relating the Killing form and the lie 
bracket (Note that $H \in \mathfrak{h}$ and $X, Y$ is as above, so by the first basic 
formula their commutator is in $\mathfrak{h}$ also):

\begin{eqnarray*}
K(H, [X, Y] ) =& - K( [X, H], Y) = K( [H, X], Y) & \text{(Invariance of Killing form)} \\
=& K( \alpha(H) X, Y) & \text{(Semisimplicity of H)} \\
=& \alpha(H) K(X, Y)
\end{eqnarray*}

Because the root $\alpha$ is nonzero, it must evaluates to nonzero on at least 
one nonzero $H$. This formula then implies that for the $X, Y$ we've picked $K(H, [X, Y]) 
\neq 0$, hence $[X, Y] \neq 0$ (The Killing form must be zero if any one of its argument is 
zero).

Here is the long awaited step: By the non-degeneracy of the Killing form over 
$\mathfrak{h}$, there is always a unique $h_\alpha \in \mathfrak{h}$ such that $K(H, h_\alpha) 
= \alpha(H)$ for all $H \in \mathfrak{h}$, via the finite dimensional Riesz Representation 
Theorem. Substituting into our formula above:

\[
K(H, [X, Y]) = K(H, h_\alpha) K(X, Y) = K(H, K(X, Y) h_\alpha)
\]

Then by non-degeneracy yet again, $[X, Y] = K(X, Y) h_\alpha$. Therefore we get 
our first significant constraint: $[ \mathfrak{g}_\alpha, \mathfrak{g}_{-\alpha} ]$ 
is always of dimension one.

\begin{rmk}
As further consequence of the Riesz Representation Theorem, we can also move the 
Killing Form into a symmetric bilinear form on $\mathfrak{h}^*$. Define:
\[
( \alpha, \beta) = K(h_\alpha, h_\beta) = \alpha(h_\beta) = \beta(h_\alpha)
\]
Where $\alpha \mapsto h_\alpha$ is an isomorphism from $\mathfrak{h}^*$ to 
$\mathfrak{h}$.

Moreover, as the set of roots $\Delta$ spans $\mathfrak{h}^*$, by the isomorphism so does 
the family $\{ h_\alpha \}_{\alpha \in \Delta}$ spans $\mathfrak{h}$, and any nonzero rescaling of 
such family.
\end{rmk}

Now we do some re-normalization. We assume for now that $(\alpha, \alpha) = \alpha(h_\alpha) \neq 
0$. Then we let $H_\alpha = \frac{2}{(\alpha, \alpha)} \cdot h_\alpha$, which we 
call the coroot. It has the property that $\alpha(H_\alpha) = 2$. Then by 
suitably rescaling either (or both) $X, Y$ we can make $[X, Y] = H_\alpha$. 
Let's denote such (not unique due to rescaling) triples by $X_\alpha, Y_\alpha, H_\alpha$ 
for convenience. Applying the semisimplicity of elements in $\mathfrak{h}$ we 
see that 
\begin{eqnarray*}
 \left[ H_\alpha, X_\alpha] = \text{ad} H_\alpha \cdot X_\alpha = \alpha(H_\alpha) X_\alpha 
= 2 X_\alpha & (X_\alpha \in \mathfrak{g}_\alpha) \\
 \left[ H_\alpha, Y_\alpha] = \text{ad} H_\alpha \cdot Y_\alpha = (-\alpha)(H_\alpha) Y_\alpha 
= -2 Y_\alpha & (Y_\alpha \in \mathfrak{g}_{-\alpha})
\end{eqnarray*}

Hence $(X_\alpha, Y_\alpha, H_\alpha)$ is a generator for $\mathfrak{sl}(2, 
\mathbb{C})$. We will be able to prove even more constraints on the structure of 
$\mathfrak{g}$, and the remaining root system axioms, only after introducing the 
last powerful technique: for the sake of clarity we skip ahead and state the 
results first, and also provide an interpretation:

\begin{thm}[Structures of Semisimple Lie Algebra]
\begin{enumerate}
\item All nonzero root space $\mathfrak{g}_\alpha$ has dimension one.
\item For each nonzero root $\alpha$, we can pick spanning vectors $(X_\alpha, Y_\alpha, H_\alpha)$ 
with $X_\alpha \in \mathfrak{g}_\alpha$, $Y_\alpha \in \mathfrak{g}_{-\alpha}$ (which is nontrivial 
due to axiom 2), $H_\alpha \in [\mathfrak{g}_\alpha, \mathfrak{g}_{-\alpha}]$, 
the last space is a one dimensional subspace of $\mathfrak{h}$.
\item The coroots $\{ H_\alpha \}_{\alpha \in \Delta}$ spans $\mathfrak{h}$.
\item The triples satisfies the commutator relation for $\mathfrak{sl}(2, 
\mathbb{C})$.
\item The coroot is defined by the formula $H_\alpha = \frac{2}{(\alpha, \alpha)} \cdot 
h_\alpha$, with $h_\alpha$ being the representing vector for $\alpha$, and $(\alpha, \alpha) = \alpha(h_\alpha) \neq 
0$. Moreover $\alpha(H_\alpha) = 2$.
\item For two roots $\alpha, \beta \in \Delta$ with $\alpha \neq -\beta$ we have 
$[\mathfrak{g}_\alpha, \mathfrak{g}_\beta] = \mathfrak{g}_{\alpha + \beta}$, 
with the convention of a zero space in case $\alpha + \beta \notin \Delta$.
\end{enumerate}
\end{thm}

Recalling our original programme of simultaneously diagonalizing the adjoint 
operator in CSA, here we can think of any semisimple Lie Algebra as being 
controlled by the CSA, with all root spaces being simple, and any root deriving 
a copy of the $\mathfrak{sl}(2, \mathbb{C})$, where the spanning vector in the 
root space is a raising operator, the corresponding vector in its negation a 
lowering operator, and their commutator an element in the CSA. Moreover the 
coroots derived these way, when collected over all roots, spans the CSA.

In the next section we will investigate various representations of $\mathfrak{sl}(2, \mathbb{C})$ 
hidden in suitable subspaces of $\mathfrak{g}$.

\subsection{Strings, Cartan Integers, and remaining results}

The second key technique is that of shifting eigenvalues, as we've used when 
studying representations of $\mathfrak{sl}(2, \mathbb{C})$. We will use it three 
times in total. The idea is to collect all eigenspaces that may be hit by a 
shifting operator starting from some initial space, so that together we get an 
invariant subspace and hence a representation of $\mathfrak{sl}(2, \mathbb{C})$, 
upon which we can invoke the classification results we have developed earlier. 
For convenience here is a cheatsheet:

\begin{mdframed}
\paragraph{Representations of $\mathfrak{sl}(2, \mathbb{C})$}

\begin{itemize}
\item For each positive integer $n$ there is an unique irreducible representation of $\mathfrak{sl}(2, \mathbb{C})$ 
of dimension $n+1$.
\item The eigenvalues of this representation is $\{ n, n - 2, \cdots, -n + 2, -n 
\}$, all with eigenspace of dimension one.
\item $H$ is a diagonal operator; let the eigenvector with eigenvalue $n - 2j$ 
be $v_j$ ($0 \leq j \leq n$).
\item Formula for remaining elements: $F \cdot v_j = v_{j+1}$ (0 if $j =n$), $E \cdot v_j = j (n - j + 1) 
v_{j-1}$.
\end{itemize}
\end{mdframed}

Now for the key definition:
\begin{mydef}[String]
For two roots $\alpha, \beta \in \Delta$ a $\alpha$-string through $\beta$ is the subspace
\[
\mathfrak{g}_\beta^\alpha = \oplus_{n \in \mathbb{Z}} \mathfrak{g}_{\beta + n \alpha}
\]
\end{mydef}

TODO: Degenerate case?

Here $\alpha$ is for the invariance while $\beta$ can be varied to let us 
compare the actions of $\mathfrak{sl}(2, \mathbb{C})$. This is a form of 
closure. In particular it is obvious using the basic formula at the beginning of 
this chapter that it is invariant under $(X_\alpha, Y_\alpha, H_\alpha)$. 
Henceforth let's denote the subalgebra (isomorphic to $\mathfrak{sl}(2, \mathbb{C})$) 
spanned by this triple $\mathfrak{s}_\alpha$.

\paragraph{First Use: Norm of roots are nonzero}

We want to prove that $(\alpha, \alpha) = \alpha(h_\alpha) \neq 0$. For a fixed root $\alpha$ 
let us consider the $\alpha$-string through $\beta$, letting $\beta$ varies over 
all roots. Per discussion above it is invariant under $\mathfrak{s}_\alpha$ and 
is therefore a representation. (we won't be using the heavy weight result above 
yet; this is a kind of bootstrap stage) Consider the action of $h_\alpha$ on this subspace: by 
semisimplicity it scales vectors in $\mathfrak{g}_{\beta + n \alpha}$ by 
$\beta(h_\alpha) + n \alpha(h_\alpha)$ provided $\beta + n \alpha$ is actually a 
root. Therefore on each such block the trace is just the same diagonal entry 
multipled by the dimension of that block / length of the diagonal: $N_n (\beta(h_\alpha) + n \alpha(h_\alpha))$ 
where $N_n = \text{dim}(\mathfrak{g}_{\beta + n \alpha})$. Summing over all 
blocks the trace of $\text{ad} h_\alpha |_{\mathfrak{g}_\beta^\alpha}$ is:
\begin{eqnarray*}
 & \sum_{\substack{n \in \mathbb{Z} \\ \beta + n \alpha \in \Delta}} N_n (\beta(h_\alpha) + n \alpha(h_\alpha)) \\
= & \sum_{\substack{n \in \mathbb{Z} \\ \beta + n \alpha \in \Delta}} \left( N_n \beta(h_\alpha) \right) + \left( n N_n \alpha(h_\alpha) \right) \\
= & \left( \sum_{\substack{n \in \mathbb{Z} \\ \beta + n \alpha \in \Delta}} N_n \beta(h_\alpha) \right) + q \alpha(h_\alpha) \\
= & \text{dim}(\mathfrak{g}_\beta^\alpha) \beta(h_\alpha) + q \alpha(h_\alpha)
\end{eqnarray*}

Where $q$ is some complicated thing but is assured to be an integer. Actually, 
it is possible to directly deduce that the trace above is 0: for it is the 
commutator $[ X, Y]$ (before rescaling) and by the Jacobi Identity 
\[
\text{ad} [X, Y] = [ \text{ad} X, \text{ad} Y ] = \text{ad} X \circ \text{ad} Y 
- \text{ad} Y \circ \text{ad} X
\]
Then we take trace on right hand side and note that trace is commutative (apply the cyclic shift 
property $\text{Tr}(ABC) = \text{Tr}(BCA)$ with $A = I$ say), hence the two 
terms cancel out.

Now suppose to the contrary that $\alpha(h_\alpha) = 0$. Then by the equation 
above we get that $\beta(h_\alpha) = 0$ for all nontrivial roots (since the string is then 
nontrivial as we have at least $n = 0$): by the Lemma this implies that $h_\alpha = 
0$, a contradiction.

We can actually get more from the above: the value $\beta(h_\alpha) = (\beta, \alpha)$ 
is always a rational multiple of $\alpha(h_\alpha) = (\alpha, \alpha)$.

\paragraph{Second Use: All root spaces have dimension one, and baby version of axiom 3}

Now let's consider the subspace $\mathfrak{s}_\alpha + \oplus_{n \in \mathbb{Z}^+} \mathfrak{g}_{n 
\alpha}$ with adjoint action of $\mathfrak{s}_\alpha$ as usual.
It is invariant under $\mathfrak{s}_\alpha$ which is tedious to see: $H_\alpha$ just rescale elements 
and so is invariant; $X_\alpha$ raises eigenvalue and is also okay (for $Y_\alpha$ the result is 
$H_\alpha$); finally for $Y_\alpha$ we need to pay attention to its action on 
$\mathfrak{g}_\alpha$. Although we are not sure of its dimension, we already 
know that $[\mathfrak{g}_\alpha, \mathfrak{g}_{-\alpha}]$ is one dimensional and 
per our choice $[Y_\alpha, \mathfrak{g}_{\alpha}] \in (H_\alpha)$.

By the same trick of commuting trace, we see that $H_\alpha$ has zero trace. On 
the other hand, using the same dimension counting method above, its trace is:
\[
2(-1 + n_\alpha + 2 n_{2\alpha} + 3 n_{3\alpha} + \cdots)
\]
where $n_\gamma = \text{dim}(\mathfrak{g}_\gamma)$. (The sum is well defined as finite 
dimensionality of $\mathfrak{g}$ implies that only finitely many of these spaces 
will be nontrivial and hence contribute to above) As all variables are 
non-negative integers, the only solution is $n_\alpha = 1$, $n_{k\alpha} = 0$ 
for $k = 2, 3, \cdots$ (Any of them contribute at least $k > 1$ upon jumping to 1: this makes the sum $> 
0$). We get our results, and also the nice bouns that $\mathfrak{g}_{n \alpha}$ 
is trivial for all integers besides $\pm 1$. We will strengthen this to all 
scalar multiple, not just integral, after introducing Cartan Integers.

\paragraph{Third Use: Matching Eigenvalues, Cartan Integers, and all the rest}

Finally, after a long journet we are in a position where the end goal is within 
reach. Again, consider the $\alpha$-string through $\beta$ in general. We 
already saw that it is a representation of $\mathfrak{s}_\alpha$. Hence, by 
complete reducibility it can be uniquely decomposed as the direct sum of 
irreducible representations. I claim that it is in fact already irreducible, 
that is, it has just one component in the decomposition. Our technique 
throughout will be matching the spectrum / eigenvalues.

By examining the spaces we see that the set of possible eigenvalues of $H_\alpha$ 
is $\{ \beta(H_\alpha) + k \alpha(H_\alpha) \}_{k \in \mathbb{Z}} = \{ \beta(H_\alpha) + 2k 
 \}_{k \in \mathbb{Z}}$ as $\alpha(H_\alpha) = 2$. As all eigenvalues across all 
 possible irreducible representation of $\mathfrak{sl}(2, \mathbb{C})$ are 
 integers, $\beta(H_\alpha)$ have to be an integer also. This number is the 
 Cartan Integer. We note that it can also be expressed as $\frac{2}{(\alpha, \alpha)} \cdot 
 \beta(h_\alpha) = \frac{2}{(\alpha, \alpha)} \cdot (\beta, \alpha) = \frac{2 (\beta, \alpha)}{(\alpha, 
 \alpha)}$. This establishes axiom 4 (Integrality Condition). We denote such 
 integer by $a_{\beta \alpha}$.
 
 Next, we observe that all eigenvalues of $H_\alpha$ are of the same parity (i.e. even or odd) 
 as $\beta(H_\alpha)$. Since all eigenvalues of the irreducible representation 
 of odd dimension are even and vice versa, it suffices to consider only 
 irreducible representations with compatible parity in dimension. Now, we have 
 shown in last use that all eigenvalues of $H_\alpha$ are of multiplicity one (dimension 
 of eigenspaces are all one). This rules out any case of multiple components: at 
 the very least the middlemost eigenvalue, shared by the unique irreducible 
 representations of all even / odd dimensions, will then get repeated and 
 receive a multiplicity greater than one. (For $n$ odd, 1 and -1 are shared; for $n$ 
 even, 0 is shared)
 
 Now that we've shown irreducibility of $\mathfrak{g}_\beta^\alpha$, things get 
 much easier. Macthing against the set of eigenvalues directly, we have:
 
 \begin{itemize}
\item $-n \leq a_{\beta \alpha} \leq n$.
\item Letting $p(\alpha, \beta)$ and $q(\alpha, \beta)$ be some nonnegative 
integers, the components are nontrivial exactly for $-q \leq k \leq p$. So the 
maximum eigenvalue is $a_{\beta \alpha} + 2p = n$ and the minimum eigenvalue is 
$a_{\beta \alpha} - 2q = -n$.
\item Looking at the average point / summing the two equations, $2 a_{\beta \alpha} + 2(p - q) = 0$ 
and hence $a_{\beta \alpha} = q - p$.
\end{itemize}

As a result, we can finally get axiom 3: for the reflection through the 
hyperplane perpendicular to $\alpha$, we see that
\[
\beta - \frac{2 (\alpha, \beta)}{(\alpha, \alpha)} \alpha = \beta - a_{\beta \alpha} 
\alpha
\]
is a root because $-a_{\beta \alpha} = p - q$ is indeed an integer sandwiched 
between $p$ and $q$.

Finally, we get the full form of axiom 2. By evaluating the expression, letting $\beta = c \cdot \alpha$ 
for $c \in \mathbb{C} - \{ 0 \}$,
\[
a_{\beta \alpha} = \frac{2 (c \alpha, \alpha)}{(\alpha, \alpha)} = 2c 
\]

\[
a_{\alpha \beta} = \frac{2 (\alpha, c \alpha)}{(c \alpha, c \alpha)} = \frac{2c}{c^2} 
= \frac{2}{c}
\]

As both of them are integers, the only solutions are $\pm 1$, $\pm 2$, and $\pm 
\frac{1}{2}$. The first is allowed by axiom 2. Using axiom 2 to flip the sign if 
necessary, we only need consider $2$ and $\frac{1}{2}$. 2 is not allowed by the 
baby version. $\frac{1}{2}$ is also not allowed: for in that case $\alpha = 2 \cdot (\frac{1}{2} \alpha)$ 
is an integer multiple of $\frac{1}{2} \alpha$, which is again forbidden by the 
baby version of axiom 3.

\section{Real Form}
TODO

\end{document}  
