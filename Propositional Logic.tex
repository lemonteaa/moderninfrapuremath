
\documentclass[11pt]{article}

\usepackage{ifpdf}
\ifpdf 
    \usepackage[pdftex]{graphicx}   % to include graphics
    \pdfcompresslevel=9 
    \usepackage[pdftex,     % sets up hyperref to use pdftex driver
            plainpages=false,   % allows page i and 1 to exist in the same document
            breaklinks=true,    % link texts can be broken at the end of line
            colorlinks=true,
            pdftitle=My Document
            pdfauthor=My Good Self
           ]{hyperref} 
    \usepackage{thumbpdf}
\else 
    \usepackage{graphicx}       % to include graphics
    \usepackage{hyperref}       % to simplify the use of \href
\fi 

\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{amsthm}

\newtheorem{mydef}{Definition}
\newtheorem{prop}{Proposition}
\newtheorem{thm}{Theorem}
\newtheorem{rmk}{Remark}

\title{Propositional Logic}
\author{The Author}
\date{}

\begin{document}
\maketitle

The first simple formal system we are going to study is Propositional Logic. It 
is a system focusing on the logical interrelationship between propositions, or 
truth-claims about the universe. Because of this focus, propositions themselves 
are atomic and unanalyzed, appearing as a blackbox with its internal structure 
hidden. After we've gained some experience in this simpler settings, the First 
Order Logic in next chapter will build on top of this substrate, peel back the 
blackbox, and impose a framework for describing propositions that is a better 
match for the needs of Mathematical Logic (from us the user's point of view, logic in 
service of Mathematics) than everyday logic.

We will first briefly describe its language and grammar, then look at a Natural 
Deduction (style) System. We will try to bring a higher level clarity to the 
situation, and instead of just blindly accepting the system and burying in long 
formal proofs (as those tends to be by necessity), critically examine the 
considerations behind its design, and the pros and cons from a mostly pragmatic, 
user point of view. In particular we will look at intuitionistic logic, and the 
various laws that can be added to turn it into the classical logic we usually 
have. We will show that they are all equivalent in the sense that given the base 
system, adding any one of them is enough.

After going through the syntactic side, we will turn to the semantics. 
Propositional Logic has an obvious, simple semantics, which makes it a good 
playground for beginners just getting started. Even in such a nice settings, we 
will show how the philosophical discussions in the last chapter get implemented. 
We will prove our first significant meta-theorem, and introduce the basic 
technique of structural induction, as well as that of emulating semantics in 
syntax. Both are fundamental ideas that will be repeatedly used in the field.

Conditional Proofs are a daily necessity of the real life of Mathematician, but 
they complicates the definition of a formal proof significantly. Given the 
importance of simplicity in designing formal system, it would be desirable to be 
able to get rid of it. Hilbert Style System achieves this by encoding the 
process of proof transformation into its very axioms. Hence in such system we 
can actually prove the Deduction Theorem as a meta-theorem.

Finally, equational logic has various nice properties not shared by other, more 
general system. Due to the simplicity of propositional logic, it is possible to 
outright replace it with the Boolean Algebra. We show how it can be developed 
straight from the truth value semantics and demostrate its use as an aid and 
possible alternative to conventional, proof based approach.

\section{Logical Operators and Grammar}

The Language of Propositional Logic consists of an infinite supply of 
propositions as primitive, and a small set of logical operators. Propositions 
are denoted by a single letter that identifies it.

The logical operators are: and ($\wedge$), or ($\vee$), not ($\neg$), if 
($\rightarrow$), iff/if and only if ($\leftrightarrow$), xor ($\oplus$), nand/not-and, 
and nor/not-or. All operators are binary except for not, which is unary. There 
is also the tautology ($\top$) and falsehood ($\bot$), which are nullary/0-ary, 
or in other word constants. We will find some use for them later on. In many 
texts they goes by the more fancy name of conjunction, disjunction, negation, 
conditional, biconditional, etc.

Not all operators are included in the definition of the language, since some of 
them are redundant: they can be defined in terms of the others. We will see how 
to do this within the framework of meta-mathematics in later chapter (Conservative 
Extension). In fact in extreme case one operator suffices - but such compression 
is so excessive that it renders the language more like a machine language than a 
human one and so is not used in practise. For our case we only need to know that 
several popular choices exist. One is to have and, or, not. This is not 
arbitrary but because of connection with Boolean Algebra. Another choice is the 
last one plus if, and maybe also iff. This is because they are the most commonly 
used ones: if can be defined away only somewhat clumsily ($p \rightarrow q$ is 
$\neg p \vee q$), while iff is almost trivial ($p \leftrightarrow q$ just mean $(p \rightarrow q)
 \wedge (q \rightarrow p)$) but it is so common that the tradeoff is a close 
 call. (This is due to its relation with equation/identity) Yet another choice 
 is to have if, not, appearing in Hilbert Style System. This is because of the 
 fundamental importance of conditional reasoning. Not can be removed with enough 
 effort, if purity is what one's after.

People should be aware that while we use natural language in the names of these 
operators, we are working in the Mathematical Universe - their precise meaning 
differ somewhat from what one may tacitly assume as custom/culture. Natural 
language evolved in the context of human society, and human affairs are 
fundamentally messy. Such messiness, if allowed to seap in to our formal system, 
makes them harder to work with mathematically - hence why such a divergence.

In particular, or means inclusive-or: at least one of them is true. The natural 
language or is either-or: it tacitly assumes that they will not both be true, 
but logical operator does not carries such assumption - it must be able to deal 
with all situation, including ones presumed impossible. Hence if we must view 
either-or in the lens of our formal system, the closest match will be 
xor/exclusive-or: exactly one of them is true (so in case both of them are true, the 
either-or statement would be false). In a similar way conditionals in daily life 
are often meant to be evaluated only if the antecedent is true, while in our 
system we will consider the conditional to be true if the antecedent turns out 
to be false. There are many further problems that, when turned around and 
anchored on the land of linguistic and philosophy, suggests that Mathematicians' 
version of the conditional is at best an inadequate, and at worst defective and 
misleading modelling of the human concept. To give one a taste, the if in our 
system is called a material conditional, the same words in natural language, 
indicative conditional. There are also statements of causality and counterfactual 
conditionals. While we are first of all pursuing Mathematics, it does allows 
other fields to stimulate its development, hence some of the concerns listed 
above does lead to new Mathematics in the form of Non-Classical Logic to capture 
these beautiful tapestries, such as Modal Logic, Paraconsistent Logic, etc.

For this simple system, the basic rules of Grammar in the last chapter suffices. 
Thus grammatically valid sentences are called well-formed-formula, or wff. They 
admits an Abstract Syntax Tree showing its grammatical structure, which we will 
exploit later.

\section{Natural Deduction}

What Mathematicians call proofs are actually informal sketch of proof (or even just an 
indirect argument that a formal proof exists) when tested with the extreme rigor 
of formal systems. This may be surprising to beginners. Afterall Mathematical 
training at the beginning undergraduates level often emphasis proofs and rigors, 
thus it may comes as a shock that they in turn may be judged to fall short.

More experienced people will realize that these are all in harmony. Rigor is an 
important goal, for it helps ensure reliability of our arguments, acting as a 
wise check and balance given the reality that human reasonings are often faulty, 
imperfect, and clouded by ego. However it is not an absolute value nor ultimate 
goal - Mathematical Understanding is. So it have to be balanced against other 
competing goals, such as intuition, practicality (such as the length of proof - 
you may ask your local professional Mathematicians for tales of proofs spanning 
literally thousands of pages, even multiple Book Volumnes!), pedagogy (easiness of 
being understood and absorbed by learners), and so on. As a result there often 
is an appropiate level of rigor depending on context (and here we deliberately choose 
one much lower than what is considered standard - it is more important to be 
able to enjoy the music first before drilling the notes).

(Some may argue that there is a difference between rigor and formality, and that while 
too much formality is bad, it is possible to be very rigorous without 
sacrificing other things. I partially agree to this sentiment.)

Perhaps in response to the ideas above, some Logicians wanted to have a formal 
system that, as much as feasible, model the proof process as it is naturally 
done by normal people. Systems developed with this in mind is a Natural 
Deduction Style System. We present one possible system here.

We pick and, or, not, if, iff as our logical operators. There is no axioms and 
10 rules of inferences, 2 for each operators. They are shown in the table below. 
(Don't panic! We will show that it is not as hard as it seems.)



These rules are not arbitrary but organized according to a principle - that to 
sufficiently specify an axiomatic/deductive system one should completely define 
each elements. Here it means that for each operator, we must know how to make it 
and use it. So to each operator is associated two rules, one for introducing/making the 
operator, and one for eliminating/using it. To make it means to start with some 
other proved statements and deduce a new proposition that has that operator. To 
use it means to start with a statement containing the operator, and deduce 
further consequences not involving the operator.

We will now start to analyze the system as promised. First, biconditional is at 
the boundary of our system: per the discussion in last section we do not really 
need it, and the rules here is just a reiteration of our definition of iff. We 
just include it as a convinience of life, so from now on we mostly ignore it. (But see 
the practical exercise at the end of section)

The rules for and and if are simplest and also clear enough. However we see that 
introducing a conditional requires having a conditional proof of something else. 
On closer thought this brush with our formal notion of proof the wrong way: 
ideally a proof should stay within the system throughout. What we have here is 
appropiate as a meta-theorem, but as a primitive we will no longer be able to 
treat formal proof as an object entirely inside the system, unless we can, say, 
encode the notion of provability inside the system which is inadvisable. What we 
have here is essentially a formal modelling problem - we have not yet succeeded 
in formalizing the system. We will ignore this for now and only later return and 
deal with it.

The rules for or is slightly less obvious, but it is still okay. Or-elimination 
is just proof-by-case. Or-introduction is somewhat weaker than we want - it is 
entirely possible (and probably the whole point of using or) that different 
branch of the alternatives are true depending on the situation, while the rule 
here forces us to be definite about which one of them is true. Still, these 
weaknesses are not fatal.

Finally we come to not, which is the trickiest and also important part.

\section{Truth Value Semantics}

\section{Completeness Theorem}

The main result in this chapter is also our first major theorem:

\begin{thm}[Completeness of Propositional Logic]
Let $\phi$ be a wff over the variables $V$. If $S \models \phi$ for all models $S$ supported 
on $V$, then $\vdash \phi$.
\end{thm}
\begin{proof}
The main idea is, as the introductory remarks mention, emulating semantics (truth table) 
on the syntactic side. To help ease understanding we break it down into 
digestable steps:

Step 1: We state and prove the Unification Lemma:

\begin{enumerate}
\item If $\Gamma, x \vdash \phi$ and $\Gamma, \neg x \vdash \phi$, then $\Gamma \vdash 
\phi$.
\item Define a function $t$ mapping a model $S$ to a list of propositions, one for 
each variable $v$ in $V$ of the form $v$ if $v$ is true in $S$ and $\neg v$ 
otherwise. Then if $t(S) \vdash \phi$ for all model $S$, $\vdash \phi$.
\end{enumerate}

The meaning here is that we perform model checking on the truth table 
row-by-row: each row correspond to one model/truth-assignment, that we add as 
the premises. If we can syntactically prove $\phi$ in all emulations, then we 
can transform to an unconditional proof of $\phi$ through this lemma, corresponding to the idea 
that a proposition is actually true iff it is true under all models. The term 
unify has a special meaning in logic: here it means to branch on possibility of 
a variable and then to merge them.

We now prove this lemma. For 1, use the deduction theorem to get a conditional 
proof of $x \rightarrow \phi$ and $\neg x \rightarrow \phi$ in context $\Gamma$. 
Then use proof by case and the law of excluded middle. For 2, use 
induction/iteration on the variables: apply 1 to the last variable, matching $\Gamma$ 
to each unqiue combination of the remaining variables. As $t(S)$ enumerates all $2^n$ 
combinations of the $n$ variables, after this we are reduced to all $2^{n-1}$ 
combinations of the remaining $n-1$ variables. Repeatedly do this and we're 
done.

Step 2: Emulate the semantics. Define the syntactic interpretation function: For 
$S$ a model and $\phi$ a wff, let

\[
\mathcal{I}_S(\phi) =
\begin{cases}
\phi ,& \text{if} S \models \phi \\
\neg \phi ,& \text{if not}
\end{cases}
\]

By flipping the formula as needed, this ensures that the resulting proposition 
is always true in that model.

Next, we introduce the technique of structural induction. While ordinary 
induction works on integers or more generally total orders, structural induction 
induct on the structure of an entity. In our case, formulas have an abstract 
syntax tree encoding its syntactical structure, and we try to show that 
properties holding over smaller trees can be combined to hold over the composite 
tree. Equivalently, we break down the tree at the root node, and show that if a 
property holds over each of the subtree/sub-formula, it holds over the whole 
tree also. Since formulas and hence their associated trees are finite, this will 
always terminates.

As our grammar is defined recursively with cases, we will also need to branch 
and prove each of those case in our induction steps. Thus structural induction 
mirrors the recursive definition of the entity in question.

To make these concepts more tangible, a wff $\phi$ is either a primitive 
variable, say $p$, or a logical operation applied over sub-formulas $\text{Op} (\psi_1, \psi_2, \cdots, 
\psi_n)$. The first case is the base case of induction: if $p$ is true then $p \vdash p$ 
is obvious; If not, $\neg p \vdash \mathcal{I}_{p = 0} (p) = \neg p$ is also 
obvious. For the second case, it suffices to test it against all possible 
combinations of truth values of the subformulas. Letting $R$ be any model over $\psi_1, \cdots, \psi_n$ 
treated as formal variables, we need to show that $R \vdash \mathcal{I}_{R}(\text{Op} (\psi_1, \psi_2, \cdots, 
\psi_n))$. (This may be a bit confusing - we will tidy it up at the last step)

Before going on, we note that what we do in the induction step is to effectively 
reproduce the truth table of the individual logical operators using syntactic 
proofs.

Step 3: Details of the structural induction step.

This is where we finally need to use knowledge of the actual axioms/propositions 
in our system. Continuing from above, we branch by the logical operation at the 
top level of the formula into cases:

\begin{description}
\item[and] If $u, v$ are both true, then so is $u \wedge v$ by conjunction 
introduction. If at least one is false, say $u$, then we prove $\neg (u \wedge v)$ 
by contradiction: if not, then by conjunction elimination $u$ is true, which 
contradicts the assumption that $u$ is false.
\item[or] If at least one of $u, v$ is true, say $u$, then so is $u \vee v$ by 
disjunction introduction. If both are false but $u \vee v$ is true, then using 
proof by case will lead to a contradiction: for instance in case of $u$ this 
contradicts $\neg u$.
\item[not] If $u$ is true, then $\neg u$ is false, thus by the construction of $\mathcal{I}_R$ 
we will need to prove $\neg \neg u$, which is done by double negation 
introduction. If $u$ is false then $\neg u$ is true, and $\neg u \vdash \neg u$ 
is trivial.
\item[if] The only case $u \rightarrow v$ is false is when $u$ is true while $v$ 
is false. Then assume to the contary that $u \rightarrow v$, by Modus ponens $v$ 
is true, contradicting $\neg v$. The case when $u \rightarrow v$ is true can be 
covered by two subcases: $u$ is false, or $v$ is true. For the first subcase, if $u, \neg u$ 
are both true the principle of explosion means we can prove anything, which we 
take to be $v$. Using deduction theorem, this means $\neg u \vdash u \rightarrow 
v$. For the second subcase we again have $v \vdash v$. As unused hypothesis has 
no impact on proof, we also have $u, v \vdash v$. Applying the deduction theorem 
leads to $v \vdash u \rightarrow v$.
\item[iff] TODO
\end{description}

Step 4: Finishing up the main argument.

We repeat the overall arguments, filling in some details. By 2 of unification 
lemma, it suffices to show that $t(S) \vdash \phi$ for all models $S$. Since the 
proposition is actually true, $S \models \phi$ for all $S$, hence $\mathcal{I}_S(\phi) = \phi$ 
always, so we are reduced to showing $t(S) \vdash \mathcal{I}_S(\phi)$, which we 
show by structural induction.

The base case is as argued in step 2. For the induction case the formula $\phi$ 
must then be of the form $\text{Op}(\psi_1, \cdots, \psi_n)$, when $\text{Op}$ 
is some logical operator and $\psi_1, \cdots, \psi_n$ are subformulas. By the 
induction hypothesis the interpreted version of each subformulas must be 
provable under the model, that is $t(S) \vdash \mathcal{I}_S(\psi_k)$ for each 
$k$. Then substituting $\mathcal{I}_S(\psi_k)$ directly into the respective 
proofs covered in step 3, selecting the right proof to use based on whether each 
$\psi_k$ is true or false under model $S$, we have $t(S) \vdash \text{Op}(\psi_1, \cdots, 
\psi_n)$, completing the induction.

\end{proof}

\section{Hilbert Style System and Deduction Theorem}

\section{Boolean Algebra}

\end{document}  
