
\documentclass[11pt]{article}

\usepackage{ifpdf}
\ifpdf 
    \usepackage[pdftex]{graphicx}   % to include graphics
    \pdfcompresslevel=9 
    \usepackage[pdftex,     % sets up hyperref to use pdftex driver
            plainpages=false,   % allows page i and 1 to exist in the same document
            breaklinks=true,    % link texts can be broken at the end of line
            colorlinks=true,
            pdftitle=My Document
            pdfauthor=My Good Self
           ]{hyperref} 
    \usepackage{thumbpdf}
\else 
    \usepackage{graphicx}       % to include graphics
    \usepackage{hyperref}       % to simplify the use of \href
\fi 

\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{amsthm}

\newtheorem{mydef}{Definition}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}

\title{Elementary Transcendantal Functions}
\author{The Author}
\date{}

\begin{document}
\maketitle

Armed with knowledges of analysis, we are now able to develop theories of 
special functions rigorously. While introductory textbooks on calculus usually 
provide definitions and prove their properties, here we will do similar things 
but with a special emphasis on boostrapping - restricting ourselves to only use parts of 
the theory of analysis that are available without assuming existence of these 
functions, in order to avoid circular arguments. In some instances this will 
mean only using more tedious, elementary method even if more intuitive methods of calculus 
appear to be available (because they aren't).

In this chapter, we will focus on elementary transcendantal functions. 
Transcendantal functions are functions that are not algebraic in nature - they 
do not satisfies any polynomially equations generically. This notion can be made 
precise using the tool of Field theory, and indeed we will explain this in depth 
in section x. Meanwhile, the word elementary would unfortunately be defined purely by 
history, custom and convention, instead of any mathematical criteria: power 
functions, exponentials and logarithmic, and (inverse) trigonometric functions 
(including the hyperbolic counterparts) are included, while presumably more 
advanced ones such as gamma, beta, bessel functions, orthogonal polynomials, and 
much more will be covered in subsequent chapters on special functions.

As we will see, a recurrent theme in this chapter is that these functions have 
multiple equivalent characterisations, and among them ordinary differential 
equations and functional equations play a central theoretic role, while series, 
limits, integrals, and inverse takes a complementary computational role.

\section{Power Function}

The power function is denoted as $b^e$, and verbally described as raising $b$ to 
the power $e$. $b$ is the base while $e$ is the exponent. We will define it by 
starting from a baby version with heavily restricted domain, and then gradually 
expand the domain until it covers the entire real line.

First, note that the obvious, intuitive meaning of repeatedly multiplying the base by itself
 works without problem if the exponents are restricted to the positive integers. 
 The baisc Law of indices holds by substitution, and both the definitions and 
 properties can be made rigorous by translating to definition by recursion/proof 
 by induction.
 
 \begin{mydef}[Power Functions on the positive integers]
 Raising any real number to a positive integer power $n$ is defined by multiplying the 
 base by itself for n times:
 $$x^n = \underbrace{ x \cdot x \cdot \ldots \cdot x}_{n \text{times}}$$
 Formally, the power function on the positive integers
 $\text{Pow}^{\mathbb{N}}_{\text{alg}}: \mathbb{R} \times \mathbb{N} \rightarrow \mathbb{R}$
 is defined:
$$
\text{Pow}^{\mathbb{N}}_{\text{alg}}(x, n) =
\begin{cases}
x \cdot \text{Pow}^{\mathbb{N}}_{\text{alg}}(x, n-1), \text{if } n > 1\\
x, \text{else}.
\end{cases}
$$
 \end{mydef}
 
 \begin{prop}[Law of Indices]
 The power function satisfies the following:
 \begin{enumerate}
\item $x^{n+m} = x^n \cdot x^m$
\item $(x^n)^m = x^{nm}$
\item $(x^n) (y^n) = (xy)^n$
\end{enumerate}
 \end{prop}
 
 \begin{proof}
 We only do (2), and leave the rest as exercise that can be solved with similar techniques. 
 Intuitively, it is true because:
 \begin{eqnarray}
(x^n)^m =& \underbrace{ (\underbrace{ x \cdot x \cdot \ldots \cdot x}_{n \text{times}} ) \cdot 
(\underbrace{ x \cdot x \cdot \ldots \cdot x}_{n \text{times}}) \cdot \ldots \cdot 
(\underbrace{ x \cdot x \cdot \ldots \cdot x}_{n \text{times}}) }_{m \text{times}} 
\\
=& \underbrace{ x \cdot x \cdot \ldots \cdot x}_{nm \text{times}}
\end{eqnarray}
For a rigorous proof, we use induction on $m$. The case $m=1$ is obvious. For 
the induction step,
\begin{eqnarray*}
\text{Pow}^{\mathbb{N}}_{\text{alg}}( \text{Pow}^{\mathbb{N}}_{\text{alg}}(x, 
n), m+1) =& \text{Pow}^{\mathbb{N}}_{\text{alg}}(x, 
n) \cdot \text{Pow}^{\mathbb{N}}_{\text{alg}}( \text{Pow}^{\mathbb{N}}_{\text{alg}}(x, 
n), m) & \text{(def. of power fcn)} \\
=& \text{Pow}^{\mathbb{N}}_{\text{alg}}(x, n) \cdot \text{Pow}^{\mathbb{N}}_{\text{alg}}(x, 
nm) & \text{(induction hyp.)} \\
=& \text{Pow}^{\mathbb{N}}_{\text{alg}}(x, n + nm) & \text{(Law of Indices 1)} 
\\
=& \text{Pow}^{\mathbb{N}}_{\text{alg}}(x, n(m+1))
\end{eqnarray*}
 \end{proof}

Next, we are going to extend it first to include zero and negative integer 
powers, and then further extend it to all rational powers. This is done using 
algebraic method: we find an extension that is consistent with an extended 
interpretation of the Law of indices.

First, letting $m = 0$ in the first law, $x^n = x^{n + 0} = x^n \cdot x^0$. Thus 
we have to define $x^0$ as 1. Then $1 = x^0 = x^{n + (-n)} = x^n \cdot x^{-n}$. 
Hence $x^{-n} = \frac{1}{x^n}$. The key insight here is that we are exploiting 
the additive group structure of the integers $(\mathbb{Z}, +)$, and the fact that the first 
law of index is essentially a group homomorphism - the argument above corresponds 
to the axioms of identity and inverse in a group.

Be careful of the domain of the base: while we can allow it to be any real 
numbers when the exponent is a positive integer, the argument above fails if the 
base is 0 - indeed $0^0$ is an (in)famous indeterminate. Further restrictions 
apply for rational exponents - in those case and beyond it is perhaps safest to 
only allow non-negative real numbers, with 0 allowed iff the power is strictly 
positive.

\begin{mydef}[Power Functions on all integers]
The power function, extended to include all integers 
$\text{Pow}^{\mathbb{Z}}_{\text{alg}}: D_{\mathbb{Z}} \rightarrow \mathbb{R}$
 is defined:
$$
\text{Pow}^{\mathbb{Z}}_{\text{alg}}(x, n) =
\begin{cases}
\text{Pow}^{\mathbb{N}}_{\text{alg}}(x, n), \text{if } n \geq 1\\
1, \text{if } n = 0\\
\frac{1}{ \text{Pow}^{\mathbb{N}}_{\text{alg}}(x, -n) }, \text{if} n \leq -1.
\end{cases}
$$
where the domain is defined as:
$D_{\mathbb{Z}} = \mathbb{R} \times \mathbb{Z} - \{ 0 \} \times \mathbb{Z}_{\leq 
0}$.
\end{mydef}

\begin{proof}
We only do (1) and leave the rest as routine exercise. Here the main technique 
is to divide into cases. Skipping the degenerate case of exponent 0, we have:
\begin{description}
\item[Both $n$ and $m$ positive] This is covered by the law of indices for 
positive integers.
\item[Both $n$ and $m$ negative] By the law of indices for positive integers, 
$x^{(-n) + (-m)} = x^{-n} \cdot x^{-m}$. Taking reciprocal of the equation gives
\begin{eqnarray}
\text{Pow}^{\mathbb{Z}}_{\text{alg}}(x, n+m) = \frac{1}{\text{Pow}^{\mathbb{N}}_{\text{alg}}(x, (-n)+(-m))} 
\\
= \frac{1}{\text{Pow}^{\mathbb{N}}_{\text{alg}}(x, -n) \cdot \text{Pow}^{\mathbb{N}}_{\text{alg}}(x, -m)} 
\\
= \frac{1}{\text{Pow}^{\mathbb{N}}_{\text{alg}}(x, -n)} \cdot \frac{1}{\text{Pow}^{\mathbb{N}}_{\text{alg}}(x, -m)} 
\\
= \text{Pow}^{\mathbb{Z}}_{\text{alg}}(x, n) \cdot \text{Pow}^{\mathbb{Z}}_{\text{alg}}(x, m)
\end{eqnarray}
\item[One of them is positive and the other negative] Assume WLOG that $n > 0$ 
and $m < 0$. We have two subcases:

(a) $n+m > 0$. Let $k = n + m$. Then $n = n + m + (-m) = k + (-m)$. By law of 
indices for positive integer $x^n = x^{k + (-m)} = x^k \cdot x^{-m}$. Moving the 
term $x^{-m}$ to the other side, $x^{n+m} = x^n \cdot \frac{1}{x^{-m}}$ which 
give us the result by applying definition of power function for negative integer 
on $\frac{1}{x^{-m}}$.

(b) $n + m < 0$. Since negative numbers are a majority, we flip the sign for all 
of them. Now $x^{-(n + m)} = x^{(-m) + (-n)} = x^{-m} \cdot x^{-n} = x^{-m} \cdot \frac{1}{x^n}$ by case 
(3)(a) with role of $n, m$ switched. Take reciprocal of the equation to get the 
result.
\end{description}
\end{proof}

Going next step up the game, extension to the rationals require a small amount 
of analysis. We need the be able to take roots/surds, that is, solve the 
equation $y^n = x$ for a given $x \in \mathbb{R}^+_0$ and $n \in \mathbb{N}$. That 
a solution always exists and is unqiue is due to the fact that there exists an 
inverse function to $\text{Pow}^{\mathbb{N}}_{\text{alg}}(x, n) : [0, +\infty) \rightarrow [0, +\infty)$ 
by one version of the inverse function theorem that applies to strictly monotone 
function:

\begin{lem}
The power function for positive integers is (1) Strictly monotone increasing, (2) 
Continuous on $[0, +\infty)$, and (3) unbounded.
\end{lem}
\begin{proof}
(1): If $x > y \geq 0$, we can do induction: the base case is just the same inequality 
while for the induction step, multiply $x^k > y^k$ with $x > y \geq 0$ on both 
sides to get $x^{k+1} = x^k \cdot x > y^k \cdot y = y^{k+1}$.

(2): Apply the algebraic law of limit: as $\lim_{x \rightarrow y} x = y$ exists, 
multiply itself for $n$ times to get
\begin{equation}
\lim_{x \rightarrow y} x^n = \left( \lim_{x \rightarrow y} x \right)^n = y^n
\end{equation}

(3): If $M \leq 1$, then for any $x > 1$ we would have $x^n > 1^n = 1 \geq M$. 
For $M > 1 > 0$, assuming $n > 1$, then let $x = M$, $x^n = M^n = M^{n-1} \cdot M > 1 \cdot M = 
M$. For $n = 1$ just take say $x = 2M > M$.
\end{proof}

We usually denote the surd obtained through this method by $y = \sqrt[n]{x}$. 
Then again using the law of indices as a guide, $(x^{1/n})^n = x^{\frac{1}{n} \cdot n} = x^1 = 
x$. Then by uniqueness of the solution to the equation, $x^{1/n} = \sqrt[n]{x}$. 
Hence for a rational we can simply define $x^{n/m} = \sqrt[m]{x^n}$, where only $n$ 
is allowed to be possibly negative.

TODO: prove property

Finally, we need to extend it to all real exponent, and it is here that we 
really need analysis.

\subsection{Analytic Properties}

Our main goal is to prove that the power function is differentiable with respect 
to the base, and that the derivative is $rx^{r-1}$ for all real $r$. While one 
may be able to easily give a direct proof when $r$ is an integer or even 
a rational, the last step of generalizing to real number is difficult: the usual 
approach of continuity argument results in nested limits (see the chapter 
on Uniform Convergence for explanation of the trouble). Here we persue an 
alternative strategy of proving bounds on the power function, and then to apply 
Squeeze theorem.

Proving these inequalities is a delicate matter - many obvious methods cannot be 
used. For example, for the bernoulli inequality below we cannot use the tangent 
line argument because that would require knowing the derivative of the power 
function in general, when we are counting on this inequality to prove the 
derivative of the power function in the first place! (We also note that if one only need to
use it for the case that the exponent is a positive integer, a basic induction argument,
or expansion by binomial theorem, works)

%Moreover, we will use these results 
%again in developing the theory for other transcendantal functions. Therefore we 
%will take a detour somewhat and do them first.
%We are now ready to tackle the original problem:

\begin{prop}[Derivative of power function]
\begin{enumerate}
\item For all nonzero real $r$, $\lim_{h \rightarrow 0} \frac{(1+h)^r - 1}{h} = r$
\item The power function is differentiable on $(0, +\infty)$ with derivative 
given by $rx^{r-1}$.
\end{enumerate}
\end{prop}

\begin{proof}
\begin{enumerate}
\item We will proceed in steps.

Step 0: Case when $r \geq 1$. We have the inequalities when $x$ is close to 0:
$$
\frac{1}{1-rx} \geq (1+x)^r \geq 1 + rx
$$
The second one is just Bernoulli inequality. For the first one, rearrange to:
\begin{eqnarray*}
(1 - rx)(1+x)^r \leq& \left( \frac{(1 - rx) + r(1+x)}{r+1} \right)^{r+1} = 1 \text{(AM-GM)}
\end{eqnarray*}

Assuming $h > 0$, the ansatz is then lower bounded by $\frac{1}{h} \left[ (1 + rh) - 1 \right] = r$ 
and upper bounded by $\frac{1}{h} \left[ \frac{1}{1 - rh} - 1 \right] = \frac{1}{h(1-rh)} \left[ 1 - (1 - rh) \right] = \frac{r}{1-rh}$ 
, then results follow by Squeeze theorem. The argument for $h < 0$ is exactly 
the same with the upper and lower bound swapped.

Step 1: Case when $0 < r < 1$. Let $q = \frac{1}{r} > 1$. By applying argument of (2), 
the function $x \rightarrow x^q$ on $(0, +\infty)$ is differentiable. Hence by 
the differentiable version of the inverse function theorem the function $x \rightarrow x^r$ 
is differentiable at 1 with derivative given by $\frac{1}{q} = r$.

Step 2: Case when $r < 0$. Apply quotient rule of differentiation on 
$\frac{1}{x^{-r}}$.

\item Just evaluate the definition of derivative, factoring out $x$:
\begin{eqnarray*}
\frac{(x + h)^r - x^r}{h} =& x^r \frac{(1 + h/x)^r - 1}{h} \\
=& x^{r-1} \frac{(1 + h/x)^r - 1}{h/x}
\end{eqnarray*}
Then replace $h$ by $h/x$ as the limiting variable, and apply 1.
\end{enumerate}
\end{proof}

\section{Exponential and Logarithmic Function}

\subsection{Characterisation by Analysis}
Having clarified the motivation and some general points about the exponential 
function, we can now begin to define it and study its properties:

\begin{prop}[ODE characterisation of exponential function]
There exists an unique global solution to the initial value problem
\begin{equation}
y' = \lambda y, y(0) = 1
\end{equation}
for all real values of $\lambda$. The domain of definition is $\mathbb{R}$, and 
it is a strictly positive function, monotone increasing when $\lambda > 0$. Letting the unique 
solution for $\lambda = 1$ be denoted by $\exp$, the solution for general 
values of $\lambda$ is $\exp(\lambda x)$.

The expoential function $\exp$ admits a series representation:
\begin{equation}
\exp(x) = \sum_{k=0}^\infty \frac{x^k}{k!}
\end{equation}
and also a limit formula:
\begin{equation}
\exp(x) = \lim_{n \rightarrow \infty} \left( 1 + \frac{x}{n} \right)^n
\end{equation}

The exponential function defined this way coincides with the one defined through 
algebra (see next section): it satisfies the same algebraic law $\exp(x + y) = \exp(x) \exp(y)$ 
for all real $x, y$.
\end{prop}

\begin{proof}
By the general theory of ODE, there exists a unqiue local solution, given by the 
series:
\begin{equation}
\sum_{k=0}^{\infty} \frac{\lambda^k}{k!} x^k
\end{equation}
by applying the test, this series have an infinite radius of convergence for all 
$\lambda \in \mathbb{R}$, hence the claim of global existence and uniqueness 
follows. The claim for both the substitution formula $\exp(\lambda x)$ and the 
series follows by doing suitable substitution on the equation above. The limit 
formula follows by applying Euler's numerical method to the ODE.

For the claim of positivity and monotonicity, by using the formula $\exp(\lambda x)$ 
it suffices to only prove it for $\exp$. It is obviously strictly positive for $x > 0$ 
as each terms in the series also is. For $x \leq 0$, we will need to use a weaker form of 
the algebraic law where at least one of $x, y$ are restricted to be strictly 
positive: $1 = \exp(x + (-x)) = \exp(x) \exp(-x) \Rightarrow \exp(x) > 0$ since 
we have already shown that $\exp(-x) > 0$. Once we have proved this, 
monotonicity follows since then $y' > 0$ always (apply the ODE), and such a 
function is monotone increasing by mean value theorem.

Finally, for the algebraic law, the basic argument is to construct a new 
function:
$$
G_y(x) = \frac{\exp(x + y)}{\exp(y)}
$$
where $y$ is fixed. This is well-defined as long as $\exp(y) > 0$. Then we show 
that it satisfies the same initial value problem/ODE with $\lambda = 1$, hence 
by uniqueness $G_y(x) = \exp(x)$ for all $x$. We then finish our argument by 
applying it twice: for the first time the precondition $\exp (y) > 0$ is 
satisfied since we set $y = -x \geq 0$. From this the weak form of the algebraic law 
required to bootstrap the positivity of the exponential function is proved. 
After this is established, apply it for a second time with full generality, 
using the positivity just proved to satisfy the precondition.
\end{proof}

\subsection{Characterisation by Algebra}

\begin{prop}[Functional equation characterisation of exponential function]
For each $a > 0$, there exists an unique solution to the functional equation
\begin{equation}
f(x + y) = f(x) f(y), f(1) = a
\end{equation}
with domain being the set of all rational numbers $\mathbb{Q}$. There exists a 
unqiue extension of this solution to the set of all real numbers $\mathbb{R}$ 
such that it is continuous at $0$. Moreover any extension is everywhere 
continuous iff it is continuous at $0$.

Henceforth we only consider the unique, everywhere continuous solution. It is 
given by the fully general power function that is defined for all real exponents, 
constructed in last section: $f(x) = a^x$.

This function is strictly positive, monotone increasing if $a > 1$, and convex. Moreover, 
its left and right hand side subderivative coincides and hence it is differentiable 
at 0. Also, it is in fact everywhere differentiable, and satisfies the same ODE in 
last subsection, with parameter given by $\lambda = \lim_{h \rightarrow 0} \frac{a^h - 1}{h}$.
\end{prop}

\begin{proof}
The existence and uniqueness of solution on $\mathbb{Q}$ is shown by following 
the arguments used to develop the power function using algebraic construction in the last 
section. Existence of (continuous) extension to $\mathbb{R}$ is also shown by 
following the remaining argument of taking limit over a Cauchy sequence of 
rationals $r_n \in \mathbb{Q}$ converging to $r$ and checking the required 
properties - in particular the full functional equation is simply the law of 
indices proved by a continuity argument.

For uniqueness of continuous extension and related claim, notes that the 
condition for continuity at any point can be reduced:
\begin{equation}
f(x) = \lim_{y \rightarrow x} f(y) = \lim_{h \rightarrow 0} f(x+h) = f(x) \left( \lim_{h \rightarrow 0} f(h) \right)
\end{equation}
Which is true iff $f(x) = 0$ or the limit $\lim_{h \rightarrow 0} f(h)$ exists 
and is equal to 1. Since $f(0) = 1$, if $f$ is continuous at 0, then the limit 
exits and so $f$ is continuous everywhere; conversely, $f$ being continuous 
everywhere would also include continuity at 0.

The claim of positivity and monotonicity follows from first checking them over 
the rationals using law of indices and elementary arguments, and then extending 
them to the reals using a continuity argument. For convexity, since $f$ is 
continuous, by the theory of convex function it suffices to only check the 
special case of the inequality:
\begin{equation}
\frac{f(x) + f(y)}{2} \geq f(\frac{x + y}{2}) \Leftrightarrow \frac{a^x + a^y}{2} 
\geq a^{\frac{x+y}{2}} = ( a^{x+y} )^{1/2} = \sqrt{ a^x a^y }
\end{equation}
After substituting $u = a^x$, $v = a^y$, this is the easy case of the AM-GM 
inequality and admits a standalone proof from expanding $(u-v)^2 \geq 0$. The 
inequality is strict since it is strictly monotone, so $x \neq y$ implies $u \neq 
v$.

As $f$ is convex, by the theory of convex function, its left and right secants 
are monotone, and we can relate them directly:
\begin{equation}
S(0, -h) = \frac{a^{-h} - 1}{-h} = \frac{1 - a^{-h}}{h} = \frac{a^h - 1}{h a^h} 
= a^{-h} S(0, h)
\end{equation}
Since $\lim_{h \rightarrow 0} a^{-h} = 1$, the left and right subderivative are 
sandwiched to be the same, hence $f$ is in fact differentiable at 0. Then, 
differentiability elsewhere follow from the same argument:
\begin{equation}
\lim_{h \rightarrow 0} \frac{a^{x+h} - a^x}{h} = a^x \left( \lim_{h \rightarrow 0} \frac{a^{h} - 1}{h} 
\right) = a^x f'(0) = f(x) f'(0)
\end{equation}
Which is also the same as the ODE where $\lambda = f'(0) = \lim_{h \rightarrow 0} \frac{a^h - 
1}{h}$.
\end{proof}

To finish relating the two characterisations as fully equivalent, we need to 
define and use the Euler's constant and also the inverse function to 
exponential, that is the logarithmic function:

\begin{mydef}[Euler's constant]
Let Euler's constant be defined as $e = \exp (1)$. Then formula x and y applies 
to give two limit formulas.
\end{mydef}

\begin{mydef}[Logarithmic Function]
TODO
\end{mydef}

\begin{prop}[Rule of differentiation for general exponential function]
We have the limit $1 = \lim_{h \rightarrow 0} \frac{e^h - 1}{h}$. Hence for any $a > 
0$, $\ln (a) = \lim_{h \rightarrow 0} \frac{a^h - 1}{h}$, and so the two 
characterisations are fully equivalent: $\exp (x) = e^x$, and in the formula 
relating the two $\lambda = \ln (a)$, and $a = e^\lambda$. Moreover the 
derivative of the general exponential function is given by
\begin{equation}
\frac{d}{dx} a^x = (\ln a) a^x
\end{equation}
\end{prop}

\section{Appendix}

\subsection{Bootstrapping elementary inequalities}
Calculus is a major technique for proving inequalities. However, since we rely 
on some inequalities for developing the theory of calculus, we have to be 
careful to avoid circular reasoning, and one way to go is to prove the basic 
inequalities using elementary means only.

In this subsection, our flow will be to show how to derive the two variable 
AM-GM inequality with real valued weights from the discrete, equal weight 
version with n variables. The same inequality with both real valued weights and 
n variables can also be derived after that, although we will not need that 
result.

Next, we show that both Young's inequality and then the bernoulli inequality can 
be derived from real weighted AM-GM.

\begin{prop}[Equal weights AM-GM]
For $x_1, x_2, \cdots x_n \geq 0$, we have:
\begin{equation}
\frac{x_1 + x_2 + \cdots + x_n}{n} \geq \sqrt[n]{x_1 x_2 \cdots x_n}
\end{equation}
\end{prop}
\begin{proof}
We use a special kind of induction tailor made for this problem. Some textbook 
at the high school level calls it forward backward induction, although I personally 
think no name is needed.

The base case $n=2$ can be reduced to the basic inequality $(\sqrt{x_1} - \sqrt{x_2})^2 \geq 
0$.

By repeatedly substituting the inductive assumption into the base case, we can 
prove it for $n$ any integer power of 2. For instance,
\begin{eqnarray*}
\frac{x_1 + x_2 + \cdots + x_8}{8} 
& = \frac{1}{2} \left( \frac{x_1 + x_2 + x_3+ x_4}{4} + 
\frac{x_5 + x_6 + x_7+ x_8}{4} \right) \\
& \geq \frac{1}{2} \left( \sqrt[4]{x_1 x_2 x_3 x_4} + \sqrt[4]{x_5 x_6 x_7 x_8} \right) 
\\
& \geq \sqrt{ \sqrt[4]{x_1 x_2 x_3 x_4} \cdot \sqrt[4]{x_5 x_6 x_7 x_8} } \\
& = \sqrt[8]{x_1 x_2 \cdots x_8}
\end{eqnarray*}

Finally, by backfilling excess value with the target AM, we can prove the 
inequality for any value of $n$ in the gap between two successive power, by 
invoking the inequality for $n = 2^m$ such that $k < 2^m$:

\begin{eqnarray*}
\alpha
& = \frac{x_1 + \cdots + x_k}{k} \\
& = \frac{(x_1 + \cdots + x_k) + x_{k+1} + \cdots + x_{n} }{n}, 
(x_{k+1} = \cdots = x_n = \frac{x_1 + \cdots + x_k}{k} = \alpha) \\
&\geq \sqrt[n]{x_1 \cdots x_k \cdot x_{k+1} \cdots x_n} \\
&= \sqrt[n]{x_1 \cdots x_k \cdot \alpha^{n-k}}
\end{eqnarray*}
Rearrange the inequality to get
\begin{equation}
\alpha^n \geq x_1 \cdots x_k \cdot \alpha^{n-k} \Rightarrow \alpha \geq \sqrt[k]{x_1 \cdots x_k}
\end{equation}
As needed.
\end{proof}

\begin{prop}[Real valued weighing for AM-GM]
Let $x_1, \cdots x_n \geq 0$, and $w_1, \cdots w_n$ be such that for all i, $w_i \in 
\mathbb{R}$, $w_i > 0$, and $w_1 + \cdots + w_n = 1$.
Then:
\begin{enumerate}
\item $w_1 x_1 + w_2 x_2 \geq x_1^{w_1} x_2^{w_2}$
\item $w_1 x_1 + \cdots + w_n x_n \geq x_1^{w_1} x_2^{w_2} \cdots x_n^{w_n}$
\end{enumerate}
\end{prop}
\begin{proof}
First consider the case when there are two variables and the weights are 
rational. By clearing the denominators of the weights and using $w_1 + w_2 = 1$, 
we get integers $p, q$ such that $w_1 = \frac{p}{p+q}, w_2 = \frac{q}{p+q}$. The 
inequality is then equivalent to the equal weighing AM-GM with $p+q$ terms, 
where the first $p$ variables are $x_1$ and the remaining $q$ are $x_2$.

The case for irrational weighs then follow by taking a sequence of rational 
approximation of the weights that covnerge to the actual weights, use the case 
above, and taking limits on both side of the inequality. This works because we 
had directly shown that the power function for general exponent is continuous.

The $n$-variable case follows from induction:
\begin{eqnarray*}
& w_1 x_1 + \cdots + w_n x_n + w_{n+1} x_{n+1} \\
= & w (w_1' x_1 + \cdots + w_n' x_n) + (1-w) x_{n+1} \\
& ( w = w_1 + \cdots + w_n = 1 - w_{n+1}, w_i' = w_i/w ) \\
\geq & \left( x_1^{w_1'} \cdots x_n^{w_n'} \right)^{w} x_{n+1}^{1-w} \\
= & x_1^{w_1' w} \cdots x_n^{w_n' w} x_{n+1}^{w_{n+1}} \\
= & x_1^{w_1} x_2^{w_2} \cdots x_{n+1}^{w_{n+1}}
\end{eqnarray*}
\end{proof}

\begin{prop}[Other elementary inequalities]
From the real valued weighting AM-GM inequality, we can derive:
\begin{description}
\item[Young's inequality] $\frac{1}{u} x^u + \frac{1}{v} y^v \geq xy, \frac{1}{u} + \frac{1}{v} = 1$
\item[Bernoulli inequality] $(1+x)^r \geq 1 + rx, r \geq 1, x \geq -1$
\end{description}
\end{prop}
\begin{proof}
Apply AM-GM with $w_1 = \frac{1}{u}, w_2 = \frac{1}{v}$:
\begin{equation*}
\frac{1}{u} x^u + \frac{1}{v} y^v \geq (x^u)^{1/u} (y^v)^{1/v} = x^{u \cdot 1/u} 
y^{v \cdot 1/v} = xy
\end{equation*}
Note that for both weights to be nonnegative, we must have $u, v \geq 1$.

Now substitute into Young's inequality with $u=r$, so that $\frac{1}{v} = 1 - \frac{1}{r}$:
\begin{eqnarray*}
\frac{1}{r} (1+x)^r + \left( 1 - \frac{1}{r} \right) \cdot 1 \geq 1+x \\
\frac{1}{r} (1+x)^r - \frac{1}{r} \geq x \\
(1+x)^r \geq 1 + rx
\end{eqnarray*}
\end{proof}

\subsection{Theory of Convex Function in One variable}

In this subsection, we prove the propositions in convex analysis used in the 
proof of differentiability of the exponential function defined via algebraic 
method. Since we are dealing with the special case of one real variable, we will 
be able to simplify some of the arguments. We present here a steam rolled, bare 
minimal version.

Let $I \subset \mathbb{R}$ be an interval. A function $f: I \rightarrow \mathbb{R}$ 
is convex if for any $x, y \in I$ and real valued weight $\lambda \in 
\mathbb{R}$, $0 \leq \lambda \leq 1$,

\begin{equation}
f(\lambda x + (1 - \lambda) y) \leq \lambda f(x) + (1 - \lambda) f(y)
\end{equation}

\begin{prop}
Let $f: I \rightarrow \mathbb{R}$ be a function that is continuous and midpoint convex:
\begin{equation*}
f(\frac{x_1+x_2}{2}) \leq \frac{f(x_1) + f(x_2)}{2}
\end{equation*}
Then $f$ is convex.
\end{prop}
\begin{proof}
Our main reason for using the apparently obscure proof of the AM-GM by 
backward-forward induction is that it generalizes almost directly to a proof of 
this proposition. We only highlight some of the steps that may not be 
immediately clear:

\begin{itemize}
\item One new element in the proof is the need to check that all values lie in 
$I$. For any finite set of points $x_1, \cdots, x_n \in I$, their convex 
combination $w_1 x_1 + \cdots + w_n x_n$ also lies in $I$.
\item For the backward induction step:
\begin{eqnarray*}
f(\frac{x_1 + \cdots + x_k}{k}) & = f( \frac{x_1 + \cdots + x_k + (n - k) \alpha }{n} ) 
\\
& \leq \frac{ f(x_1) + \cdots + f(x_k) + (n-k) f(\alpha) }{n} \\
& \rightarrow \\
\left( 1 - \frac{n-k}{n} \right) f(\alpha) & \leq \frac{1}{n} ( f(x_1) + \cdots + f(x_k) ) 
\\
( n - (n-k) ) f(\alpha) & \leq f(x_1) + \cdots + f(x_k) \\
f(\alpha) & \leq \frac{ f(x_1) + \cdots + f(x_k) }{k}
\end{eqnarray*}
\item The continuity of $f$ is used in the last step, after we proved the 
inequality for rational value of $\lambda$. Let $\lambda_n$ be a sequence of 
rational weights converging to $\lambda$, then $\lambda_n x + (1-\lambda_n) y$ 
converges to $\lambda x + (1-\lambda) y$, hence $f(\lambda_n x + (1-\lambda_n) y ) 
\rightarrow f(\lambda x + (1-\lambda) y)$.
\end{itemize}

\end{proof}

Given a convex function, we use $S(a, b)$ to denote the slope of the secant line 
passing through the point $x = a$ and $x = b$ on the graph of $f$. That is,
\begin{equation*}
S(a, b) := \frac{f(b) - f(a)}{b - a}
\end{equation*}

\begin{prop}[Property of secants, one-sided derivatives]
\begin{enumerate}
\item For any $a, b, c \in I$ such that $a < b < c$, $S(a, b) \leq S(a, c) \leq S(b, 
c)$.
\item $S$ is a monotone function when one of the argument is held fixed. That 
is, for fixed $a, b \in I$, and any $x, y \in I$, $x < y$, $S(a, x) \leq S(a, y)$ 
and $S(x, b) \leq S(y, b)$.
\item Both one sided derivative of $f$ exists, and is given by the formula:
\begin{eqnarray*}
f^{+}(x) &= \inf_{x < y} S(x, y) \\
f^{-}(x) &= \sup_{y < x} S(y, x)
\end{eqnarray*}
Moreover, we have $f^{+}(x) \geq f^{-}(x)$. Hence, if one can show the reverse 
inequality, then the two one sided inequalities coincides and $f$ is 
differentiable at $x$.
\end{enumerate}
\end{prop}
\begin{proof}
\begin{enumerate}
\item Conceptually, this follows from the implication of the inequality: that if 
we take the graph of $f$, take the three points $x = a$, $x = b$, and $x = c$, 
and draw a triangle joining these points, it will point downward as shown in the 
figure.

Formally, let $\lambda = \frac{c - b}{c - a}$ (So $1-\lambda = \frac{b - a}{c - a}$). 
Then $\lambda a + (1-\lambda) c = b$. The inequality implies that 
$f(b) \leq \lambda f(a) + (1-\lambda) f(c)$.

We can rearrange it in two ways: first,
\begin{eqnarray*}
f(b) - f(a) &\leq (\lambda - 1) f(a) + (1 - \lambda) f(c) = (1-\lambda) (f(c) - f(a) )\\
f(b) - f(a) & \leq \frac{b - a}{c - a} (f(c) - f(a))\\
S(a, b) &\leq S(a, c)
\end{eqnarray*}
Secondly,
\begin{eqnarray*}
-\lambda f(a) + \lambda f(c) &\leq f(c) - f(b)\\
\frac{c - b}{c - a} (f(c) - f(a)) &\leq f(c) - f(b)\\
S(a, c) \leq S(b, c)
\end{eqnarray*}

\item This follows from applying part 1 of this proposition. We divide into cases:

$a < x < y$. Then set $b = x, c = y$, and use $S(a, b) \leq S(a, c)$.

$x < a < y$: Set $a = x, b = a, c = y$, and use $S(a, b) \leq S(b, c)$.

$x < y < a$: Set $a = x, b = y, c = a$, and use $S(a, c) \leq S(b, c)$.

The other inequality is proved exactly analogously.

\item The definitions of one sided inequalities are:
\begin{equation*}
f^{+}(x) = \lim_{y \rightarrow x^{+}} S(x, y), f^{-}(x) = \lim_{y \rightarrow x^{-}} 
S(y, x)
\end{equation*}
Because of monotonicity of $S(x, y)$ from part 2, the formula does hold provided 
that we can show the two limits exist. For this it suffices to show boundedness. 
But the monotonicity holds even when the remaining argument of $S$ passes over 
the point being fixed, so any finite slope on the other side serves as such a 
bound. This also shows that all elements in the set $\{ S(x, y) \}_{x < y}$ is 
greater than or equals to $\{ S(y, x) \}_{y < x}$, hence the claim $f^{+}(x) \geq f^{-}(x)$ 
follows. The last sentence is true in one real variable as ordinary limit allows 
approaching from both directions.
\end{enumerate}
\end{proof}


\end{document}  
